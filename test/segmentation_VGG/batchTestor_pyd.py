from hairMatting import *
from hairTools import *
import os
import sys
#experiment = "alexnet_finetune_mask_deconv_alexnet_55_no_pooling_hair_random"
#experiment = "alexnet_finetune_mask_deconv_alexnet_227_no_pooling_hair_random"
#experiment = "alexnet_finetune_mask_deconv_alexnet_227_no_pooling_1fc_hair_random"
#xperiment = "alexnet_finetune_mask_deconv_alexnet_27_no_pooling_lfw_image"
experiment = "VGG_mask_mixture_2_hair_random_file_and_mask"
class Test:

	def __init__(self, snapshot = ""):
		self.m = HairMatting(snapshot = snapshot)
	
	#if_gray:0,not, 1:yes, 2:both
	def batch_test(self, folder, if_gray):
		file_list = os.listdir(folder)
		os.chdir(folder)
		result_path = "../pyd_" + folder + "_" + experiment
		check_or_create(result_path)
		for f in file_list:
			self.m.matting_pyd(f, output = result_path)


t = Test(snapshot = experiment)
t.batch_test("test_set", False)
			
			
		
		
		
