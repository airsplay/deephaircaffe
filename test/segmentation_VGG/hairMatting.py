from analysis import *
from hairTools import *
import bounding_box
import shutil
import re
import numpy as np
import matplotlib.pyplot as plt
import os
import sys
import cv2 as cv
import caffe
import face_detect
experiment = "VGG_mask_mixture_2_hair_random_file_and_mask"
class HairMatting(Analysis):
	
	def __init__(self, *args, **kwargs):
		#print "init"
		Analysis.__init__(self, *args, **kwargs)
		self.result_path_tmp = "%s.matting"
		self.fd = face_detect.FaceDetector()
		
		#print "Using prototxt %s \n and caffemodel %s" % args

	def load_data(self, data):
		
		#print data.shape
		if len(data.shape) > 2:

			data = data.astype(np.float16)
			mean = np.array([104.0, 117.0, 123.0])
			data = np.subtract(data, mean)

			data = data.transpose(2,0,1)	#Set channel
			print data.shape
			self.net.blobs['data'].reshape(1,  *data.shape)	# Reshape the data blob

			
			self.net.blobs['data'].data[...] = data			# Put the data into blob
		self.net.forward(start = 'conv1_1')
		#self.show_blobs_shape()

	def matting_pyd(self, file_name, output = "./"):
		result_path = self.result_path_tmp % (file_name)
		result_path = os.path.join(output, result_path)
		check_or_create(result_path)
		img = cv.imread(file_name)
		n = img.shape[1]
		m = img.shape[0]
		shape = (n, m)
		mask = np.zeros((m,n))
		mask = mask.astype(np.float16)
		#os.chdir(result_path)
		for size in range(160, 400, 8):
			img_tmp = adjust(img, size)
			img_tmp = near8(img_tmp)
			#img_tmp_name = file_name + str(size) + ".png"
			#save_image(img_tmp_name, img_tmp)
			self.load_data(img_tmp)
			mask_tmp = self.net.blobs['out_1'].data[0][0]
			#mask_tmp = self.matting_rgb(img_tmp_name, "./", analysis = False, face = False)
			mask_tmp = cv.resize(mask_tmp ,shape)
			mask = np.maximum(mask, mask_tmp)

		#show_image(mask)

		mask = (mask>0).astype(np.uint8)
		mask = np.tile(mask, (3,1)).reshape((3,) + mask.shape).transpose(1,2,0)
		masked = np.minimum(mask * 255, img)

		save_image(os.path.join(output, file_name + ".matting.png"), masked)
			
		
		
	# param:
	# output: The folder of analysis result
	# gray_input: the input image
	# face: use face to normalize the photo
	def matting_rgb(self, file_name, output = "./", gray_input = False, renew = True, analysis = False, face = True):
		result_path = self.result_path_tmp % (file_name)
		result_path = os.path.join(output, result_path)
		if not(os.path.exists(result_path)):
			os.mkdir(result_path)	
		else:
			if not(renew):
				return
		image = self.process(file_name, gray_input, face)
		self.load_data(image)
		mask = self.net.blobs['out_1'].data[0][0]
		#return mask
		save_image(os.path.join(output, file_name + "probability.exr"), mask)
		
		save_image_plt(os.path.join(output, file_name + "probability_plt.png"), mask)
		mask = cv.resize(mask, image.shape[:2][::-1])
		#n = int(mask.size ** 0.5)
		#mask = mask.reshape(n, n)
		#show_image_plt(mask)
		mask = sigmoid(mask)
		discrete_mask = np.round(mask)
		print "shape of discrete_mask", discrete_mask.shape
		discrete_mask = np.tile(discrete_mask, (3,1)).reshape((3,) + discrete_mask.shape).transpose(1,2,0)
		#for i in xrange(3):
			#show_image(discrete_mask[i])
		
		#show_image(discrete_mask)

		#exit()
		#analysis = True
		if analysis:
			self.save_raw_input(result_path)
			self.save_input(result_path)
			self.save_conv_blobs(result_path)
			save_image(os.path.join(result_path, "mask.png"), mask)
			save_image(os.path.join(result_path, "discrete_mask.png"), discrete_mask)	

		#print image.shape
		discrete_mask = discrete_mask.astype(np.uint8)
		#analyse_image(discrete_mask * 255)
		#analyse_image(image)
		#show_image(image)
		mat = np.minimum(discrete_mask*255, image)
		#print discrete_mask.type, image.type
		#mat = image[np.argwhere(discrete_mask == 0)]
		#image.copyTo(mat, mask)	
		#analyse_image(mat)
		if False:
			show_image(image)
			show_image(discrete_mask)
			show_image(mat)
		#print mat.shape
		if analysis:
			save_image(os.path.join(result_path, "matting.png"), mat)
			
		save_image(os.path.join(output, file_name + ".probability.png"), mask * 255)
		save_image(os.path.join(output, file_name + ".matting.png"), mat)
		save_image(os.path.join(output, file_name), image)

		
	def process(self, file_name, gray_input, face = True, size = 224):
		image = cv.imread(file_name)
		#save_image("origin_" + file_name, image)
		print "The origin shape of image is" , image.shape
		if gray_input:
			image = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
				
		if face:
			image = self.fd.auto_resize(image, size)	# Use the size of face to resize the image to a scale of 224
		image = near8(image)
		#image = cv.resize(image, (224, 224))
		
		#show_image(image)
		#pad_size = 0
		#image = cv.resize(image, (227 - pad_size * 2, 227 - pad_size * 2))
		#image = bounding_box.image_adjust(image)
		#image = np.pad(image, ((pad_size, pad_size), (pad_size, pad_size)), 'constant')
		
		
		#show_image(image)
		#save_image(file_name, image)
		return image
		#print imag

def to8(x):
	return x / 8 * 8
def near8(img):
	m = img.shape[0]
	n = img.shape[1]
	#print n,m
	n, m = to8(n), to8(m)
	#print n,m
	return cv.resize(img, (n,m))
def adjust(img, size):
	m = img.shape[0]
	n = img.shape[1]
	if m > n:
		m = int(m * size / n)
		n = size
	else:
		n = int(n * size / m)
		m = size
	return cv.resize(img, (n,m))
if __name__ == "__main__":
	#process("test.png")
	
	#c = HairMatting(get_prototxt(experiment), get_caffemodel(experiment))
	c = HairMatting(snapshot = experiment)
	#c.save_feature_map()
	c.matting_pyd("4-1.jpg")
