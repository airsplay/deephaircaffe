import numpy as np

class Flood_fill:
	
	def __init__(self, img):
		self.img = img
		self.label = np.zeros(self.img.shape)
		self.now_label = 0
		self.n = self.img.shape[0]
		self.m = self.img.shape[1]
		self.size = {}
		self.cnt = 0

	def _dfs(self, x, y):
		self.label[x, y] = self.now_label
		self.cnt += 1
		if x > 0 and self.img[x-1,y] > 0:
			_dfs(x-1, y)
		if x < self.n-1 and self.img[x+1, y] >0:
			_dfs(x+1, y)
		if y > 0 and self.img[x, y-1] > 0:
			_dfs(x, y-1)
		if y < self.m-1 and self.img[x, y+1] > 0:
			_dfs(x, y+1)
		
	# Input: img, a 2D ndarray
	#Return an image with label. 
	#And each label and its size
	def run(self):
		n = self.img.shape[0]
		m = self.img.shape[1]
		for i in xrange(n):
			for j in xrange(m):
				if self.img[i,j] != 0 and self.label[i,j] == 0:
					self.now_label += 1
					self.cnt = 0 
					_dfs(i, j)
					self.size[self.now_label] = self.cnt

		max_label = 0
		max_cnt = 0
		for key, value in self.size.items():
			if value > max_cnt:
				max_cnt = value
				max_label = key

		for i in xrange(n):
			for j in xrange(m):
				if self.label[i,j] != max_label:
					self.label[i,j] = 0
				else:
					self.label[i,j] = 1

		return self.label

				
