from analysis import *
import shutil
import re
import numpy as np
import matplotlib.pyplot as plt
import os
import sys
import cv2 as cv

caffe_root = "../"
model_folder = "models"
model = "alexnet_finetune"
snapshot_folder = "snapshot"
experiment = "alexnet_finetune_hair_gray"
iters = 12000

model_path = os.path.join(caffe_root, model_folder, model, "deploy.prototxt")
#model_path = "train_val.prototxt"
#caffemodel_path = os.path.join(caffe_root, snapshot_folder, "alexnet_finetune_hair_constant_20151022_all_layers", experiment+"_iter_"+str(iters)+".caffemodel")
caffemodel_path = os.path.join(caffe_root, snapshot_folder, "alexnet_finetune_hair_gray_20151101", experiment+"_iter_"+str(iters)+".caffemodel")
#caffemodel_path = "D:\\airsplay\\HairTrain\\models\\alexnet_finetune\\" + "bvlc_alexnet.caffemodel"



import caffe


class Test:
	def __init__(self, test_path, show_result):
	# The test path. Show_result indicate whether output the analysis result or just get the Accuracy
		self.test_path = test_path
		self.result_path = test_path + "_result"
		self.data_path = "D:\\airsplay\\HairTrain\\data\\hair"	# For select the typical potential result
		self.show_result = show_result

		if self.show_result:
			# Create the paths
			check_or_create(self.result_path)
			check_or_create(os.path.join(self.result_path, "AC"))
			check_or_create(os.path.join(self.result_path, "WA"))

		self.analysis = Analysis(model_path, caffemodel_path)
		self.parser = HairParser()
		self.transformer = HairLabelTransformer()

		
	
	def test(self):
		
		AC = 0
		WA = 0
		correct = True
		for file_name in os.listdir(self.test_path):
			if "mask" in file_name: # Filter all the masks
				continue

			label, cnt = self.parser.parse(file_name)		#parse the label and count from the file's name
			label = self.transformer.real2caffe(label)		#Transfer the label to [0..341] scale

			self.analysis.load_data(os.path.join(self.test_path, file_name))
			predict, potential = self.analysis.forward()

			if predict == label:
				AC += 1
				correct = True
				print "Now :", file_name, "Result : AC"
				result_path = os.path.join(self.result_path, "AC", os.path.splitext(file_name)[-1])
			else:
				WA += 1
				correct = False
				print "Now :", file_name, "Result : WA"
				result_path = os.path.join(self.result_path, "WA", os.path.splitext(file_name)[0])

			if self.show_result:
				if not(os.path.exists(result_path)):
					os.mkdir(result_path)

				self.analysis.save_input(result_path)
				self.analysis.save_conv_blobs(result_path)
				self.analysis.save_fc_blobs(result_path)
				self.analysis.save_prob(result_path)


				if not(correct):
					potential_path = os.path.join(result_path, "potential")
					check_or_create(potential_path)
					for i in xrange(5):
						label = self.transformer.caffe2real(potential[i])
						shutil.copy(os.path.join(self.data_path, self.parser.get_path(label), self.parser.get_file(label, cnt)), 
									os.path.join(potential_path, str(i) + "_" + self.parser.get_file(label, cnt)))

			print "AC :", AC, "WA :", WA, "Total :", AC+WA, "Accuracy :", AC*1.0/(AC+WA)
			print ""

		
if __name__ == "__main__":	
	source = "rand_bg"
	if (len(sys.argv) > 1):
		source = sys.argv[1]

	t = Test(source, False)
	t.test()
