from analysis import *
from hairTools import *
import shutil
import re
import numpy as np
import matplotlib.pyplot as plt
import os
import sys
import cv2 as cv

experiment = "alexnet_finetune_fixed_classifier_hair_hair_random"

class HairClassifier(Analysis):
	
	def __init__(self, *args, **kwargs):
		Analysis.__init__(self, *args, **kwargs)
		self.result_path_tmp = "%s.result"
		self.parser = HairParser()
		self.labelTransformer = HairLabelTransformer()
		self.data_path = "D:\\airsplay\\HairTrain\\data\\hair"	# For select the typical potential result
	def get_result(self):
		result = self.net.blobs['out'].data[0].argmax()
		print "Predicted class is" , result
		R = self.net.blobs['out'].data[0].argsort()[::-1]
		print "Potential", R[:8]
		return result, R
	def load_data(self, data):
		
		if isinstance(data, str):
			self.net.blobs['data'].data[...] = self.transformer.preprocess('data', caffe.io.load_image(data))
		else:
			print data.shape
			if len(data.shape) > 2:
				self.net.blobs['data'].data[...] = data.transpose(2,0,1)
			else:
				self.net.blobs['data'].data[...] = data 
			#for channel in xrange(3):
				#self.net.blobs['data'].data[0][channel] = data

		self.net.forward(start = 'conv1')
	def classify(self, file_name, output = "."):
		check_or_create(output)
		
		image = process(file_name)

		self.load_data(image)
		#self.forward()

		predict, potential = self.get_result()

		result_path = os.path.join(output, self.result_path_tmp % (file_name))

		check_or_create(result_path)

		#self.forward()
		self.save_input(result_path)
		self.save_conv_blobs(result_path)
		self.save_fc_blobs(result_path)
		self.save_prob(result_path)


		potential_path = os.path.join(result_path, "potential")
		check_or_create(potential_path)
		for i in xrange(5):
			label = self.labelTransformer.caffe2real(potential[i])
			cnt = 113
			shutil.copy(os.path.join(self.data_path, self.parser.get_path(label), self.parser.get_file(label, cnt)), 
						os.path.join(potential_path, str(i) + "_" + self.parser.get_file(label, cnt)))

def process(file_name):
	image = cv.imread(file_name)
	#save_image("origin_" + file_name, image)
	print image.shape
	#image = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
	image = cv.resize(image, (227, 227))
	return image
	#show_image(image)

if __name__ == "__main__":
	#process("test.png")
	c = HairClassifier(snapshot = experiment)
	c.classify("test.jpg")
