from hairClassify import *
from hairTools import *
import os
import shutil
import sys
import re
experiment = "alexnet_finetune_hair_random_hair_hair"
#experiment = "alexnet_finetune_mask_deconv_alexnet_227_no_pooling_hair_random"
#experiment = "alexnet_finetune_mask_deconv_alexnet_227_no_pooling_1fc_hair_random"
#experiment = "alexnet_finetune_mask_deconv_alexnet_27_no_pooling_lfw_image"
#experiment = "alexnet_finetune_mask_deconv_alexnet_55_no_pooling_mixedData7"
class Test:

	def __init__(self, snapshot = ""):
		self.c = HairClassifier(snapshot = snapshot)
	
	#if_gray:0,not, 1:yes, 2:both
	def batch_test(self, folder):
		file_list = os.listdir(folder)
		os.chdir(folder)
		result_path = "../" + folder + "_" + experiment
		check_or_create(result_path)
		for f in file_list:
			if "_hair.png" in f:
				#tmp_path = os.path.join(result_path, f)
				self.c.classify(f, output = result_path)
				m = re.search(r"(\w+)_hair.png", f)
				if m != None and os.path.exists(m.groups()[0] + ".jpg"):
					shutil.copy(m.groups()[0] + ".jpg", os.path.join(result_path,f +".result", "full.jpg"))
			if "matting" in f:
				self.c.classify(f, output = result_path)
				shutil.copy(f[:-12], os.path.join(result_path,f +".result", "full.jpg"))
			


t = Test(snapshot = experiment)
t.batch_test("data_yi")
			
			
		
		
		
