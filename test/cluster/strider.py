import numpy as np
import cv2 as cv
from tqdm import tqdm
from hairTools import *
#from hairMapper_for_strider_test import HairMapper
from analysis import *
experiment = "alexnet_finetune_cluster_hair_deconv_alexnet_55_no_pooling_hair_cluster_hair_front"
class Strider:
	# mask is a 0/1 mask
	# predict is a predict map with multiple labels
	def __init__(self, predict, mask, stride_num = 20):
		self.predict = predict
		self.mask = mask
		self.stride_num = stride_num
		self.mask_part = mask.copy()
		self.label = np.zeros(mask.shape)
	
	def stride_vertical(self):
		mask_sum = self.mask.sum(axis = 1)
		n = self.mask.shape[0]
		m = self.mask.shape[1]
		for i in xrange(n):
			step = 1.0 * mask_sum[i] / self.stride_num
			now = 0 
			now_label = 1 
			next_line = now + step
			
			for j in xrange(m):
				if self.mask[i,j] == 1:
					now += 1
					self.label[i,j] = now_label
				if now > next_line:
					now_label += 1
					next_line += step
	
	def stride_center(self):
		
		n = self.mask.shape[0]
		m = self.mask.shape[1]

		center_x = 0
		center_y = 0
		cnt = 0
		for i in xrange(n):
			for j in xrange(m):
				if self.mask[i,j] == 1:
					center_x = center_x + i
					center_y = center_y + j
					cnt += 1

		center_x = center_x / cnt
		center_y = center_y / cnt

		self.mask[center_x, center_y] = 1

		show_image_plt(self.mask.astype(np.uint8))

		for i in xrange(n):
			for j in xrange(m):
				if self.mask[i,j] == 1:
					pass	#distance = 

		
		
		print center_x, center_y

		exit()
	def stride(self):
		stride_method = "vertical"
		if stride_method == "vertical":
			self.stride_vertical()
		elif stride_method == "center":
			self.stride_center()

		self.dominate()
		return self.label
	

		
	# Use the predict and self.label to judge which type is dominate
	def dominate(self):
		predict_num = self.predict.shape[0]
		n = self.predict.shape[1]
		m = self.predict.shape[2]

		if n != self.mask.shape[0] or m != self.mask.shape[1]:
			print "Fucking error in shape"

		max_label = np.zeros((self.stride_num + 1))
		max_power = np.zeros((self.stride_num + 1))
		tmp_power = np.zeros((self.stride_num + 1))

		for predict_label in tqdm(range(predict_num)):
			for stride_iter in xrange(self.stride_num + 1):
				tmp_power[stride_iter] = 0

			for i in xrange(n):
				for j in xrange(m):
					tmp_power[self.label[i,j]] += self.predict[predict_label, i, j]

			for stride_iter in xrange(self.stride_num + 1):
				if tmp_power[stride_iter] > max_power[stride_iter]:
					max_power[stride_iter] = tmp_power[stride_iter]
					max_label[stride_iter] = predict_label

		for i in xrange(n):
			for j in xrange(m):
				if self.label[i,j] == 0:
					self.label[i,j] = -10
				else:
					self.label[i,j] = max_label[self.label[i,j]]

		#analyse_image(self.label)
		#show_image_plt(self.label)

