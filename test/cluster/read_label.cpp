#include<iostream>
#include <stdlib.h>
#include <stdio.h>

using namespace std;

int main() {

    FILE* fid = fopen("label.txt", "r");


    int n, m;
    /* n & m is the size of the image */

    const int max_len = 500;
    /* a is the strand id.
       b is the cluster id in that strand */
    int a[max_len][max_len], b[max_len][max_len];


    fscanf(fid, "%d %d\n", &n, &m);

    for (int i = 0; i < n; i++){
        for (int j = 0; j < m; j++) {
            fscanf(fid, "(%d, %d) ", &a[i][j], &b[i][j]);
        }
    }
}
