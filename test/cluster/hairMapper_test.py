from analysis import *
from hairTools import *
import matplotlib.pyplot as plt
import shutil
import bounding_box
import re
import numpy as np
import matplotlib.pyplot as plt
import os
import sys
import cv2 as cv
from strider import Strider

#experiment = "alexnet_finetune_map_hair_deconv_alexnet_55_no_pooling_hair_random_hair"
experiment = "alexnet_finetune_cluster_hair_deconv_alexnet_55_no_pooling_hair_cluster_hair"
class HairMapper(Analysis):
	
	def __init__(self, *args, **kwargs):
		Analysis.__init__(self, *args, **kwargs)
		self.result_path_tmp = "%s.result"						# The template of result folder's name
		self.parser = HairParser()
		self.labelTransformer = HairLabelTransformer()			# Transfer the label
		self.data_path = "D:\\airsplay\\HairTrain\\data\\hair"	# For selecting the typical potential result
		self.net.blobs['data'].reshape(1,3,227,227)				# One data for a test

	# Load data from 
	# 1. File if data is a string
	# 2. Numpy.array if data is so
	def load_data(self, data):
		
		if isinstance(data, str):
			self.net.blobs['data'].data[...] = self.transformer.preprocess('data', caffe.io.load_image(data))
		else:
			print data.shape
			# Judge the image as gray-scale image or an RGB image
			if len(data.shape) > 2:
				self.net.blobs['data'].data[...] = data.transpose(2,0,1)	# The gray-scale image automatically fill all the channels
			else:
				self.net.blobs['data'].data[...] = data 

		# Start forwarding after loading the data
		self.net.forward(start = 'conv1')
	
	# Get the map of an particular image and put the result in output 
	def mapping(self, hair_path,  file_path = "", output = "."):
		hair_folder, hair_name = os.path.split(hair_path)	
		check_or_create(output)		# The output of result(basic result)
		
		self.image = self.process(hair_path)
		tmp = cv.resize(self.image, (55,55))
		tmp = tmp.sum(axis = 2)
		self.mask = tmp > 0 
		self.background = (tmp == 0)
		self.load_data(self.image)


		result_path = os.path.join(output, self.result_path_tmp % (hair_name))	#The particular folder to save analysis results
		check_or_create(result_path)

		self.save_input(result_path)
		self.save_conv_blobs(result_path)
		#self.save_fc_blobs(result_path)
		#print "Analysis compelete"
		#self.save_prob(result_path)

		predict = self.net.blobs['out'].data[0]				# Still a C*W*H array while C is the probability vector before sigmoid
		self.predict = predict								# The origin array of predict
		for c in xrange(self.predict.shape[0]):
			self.predict[c] = self.crop_map(self.predict[c])
		
		show_predict(self.predict)

		#self.blur_predict()
		combine_method = "stride"
		if combine_method == "max":
			predict_map = convert_indicator_image(self.predict)		# Give a label to each pixel
		elif combine_method == "stride":
			stri = Strider(self.predict, self.mask)
			predict_map = stri.stride()
		predict_second_map = convert_second_indicator_image(self.predict)
		predict_prob = convert_probability_image(self.predict)	# Give a probability(confidential) to each pixel.

		

		save_image_plt(os.path.join(result_path, "predict_map.png"), predict_map)
		save_image_plt(os.path.join(result_path, "predict_prob.png"), predict_prob)

		predict_map = self.crop_map(predict_map)
		predict_second_map = self.crop_map(predict_second_map)
		predict_prob = self.crop(predict_prob)


		predict_map = cv.resize(predict_map, (227, 227), interpolation = cv.INTER_NEAREST)
		predict_second_map = cv.resize(predict_second_map, (227, 227), interpolation = cv.INTER_NEAREST)
		save_image_plt(os.path.join(result_path, "hair_predict_map.png"), predict_map)
		save_image_plt(os.path.join(result_path, "predict_prob.png"), predict_prob)
		#save_image_plt(os.path.join(result_path, "hair_predict_second_map.png"), predict_second_map)
		
		
		
		predict_part_map = np.zeros(predict_map.shape)
		import hair_cluster
		lt = hair_cluster.LabelTransfer()
		n = predict_map.shape[0]
		m = predict_map.shape[1]
		for i in xrange(n):
			for j in xrange(m):
				if predict_map[i][j] != 0:
					path, predict_part_map[i][j] = lt.unicode2part32(predict_map[i][j])

		save_image(os.path.join(result_path, "hair_predict_part_map.png"), predict_part_map)	
		

		unique, counts = np.unique(predict_map, return_counts = True)
		pair = zip(unique, counts)
		print pair
		#pair = np.sort(pair, axis = 1)[::-1] # This Sort is WRONG!!!
		pair = sorted(pair, key = lambda x:x[1])[::-1]
		print pair
		unique, counts = zip(*pair)
		print unique, counts

		"""
		tmp_image = cv.imread(os.path.join(result_path, "hair_predict_map.png"))
		#show_image(tmp_image)
		
		boxes = [bounding_box(predict_map, x) for x in unique]
		for x in xrange(len(boxes)):
		#for param in boxes:
			draw_rectangle(tmp_image, *boxes[x], color = x * 50)	
		#show_image(tmp_image)
		save_image(os.path.join(result_path, "hair_predict_map_rect.png"), tmp_image)

		graph_list = []
		for x in unique:
			path, label = lt.unicode2part32(x)
			#print path
			last = path.split("\\") [-1]
			potential_image = cv.imread(os.path.join(path, last + "_0120_mask.png"))
			#print os.path.join(path, "strands00002_0120_mask.png")
			#draw_rectangle(potential_image, *bounding_box(potential_image, label))
			highlight(potential_image, label, color = x * 50)
			show_image(potential_image)
		"""	
		
		unique = filter(lambda x:x != 0, unique)	# Erase the background
		

		
		cluster_num  = 5			# Set the number of clusters
		unique = unique[:cluster_num]	# Select the top cluster_num clusters

		"""
		predict_map = self.refine_cluster(predict_map, unique)
		predict_map = self.crop_map(predict_map)
		predict_map = cv.resize(predict_map, (227, 227), interpolation = cv.INTER_NEAREST)
		#analyse_image(predict_map)
		"""
		f, axarr = plt.subplots(3, 3)
		image_tmp = self.image.copy()
		for i in range(3):
			for j in range(3):
				axarr[i,j].axis("off")
		for x in xrange(len(unique)):
			color_h = x * 30
			# Highlight the labeling in the origin image, save it into tmp_image
			#tmp_image = cv.imread(os.path.join(result_path, "hair_predict_map.png"))
			#highlight1(tmp_image, predict_map, unique[x])
			#save_image(os.path.join(result_path, str(x) + "_position.png"), tmp_image)

			image_tmp = highlight_guided(image_tmp, predict_map == unique[x], color_h)
			axarr[x / 3 + 1, x % 3].imshow(predict[unique[x]])
			#axarr[x / 3 + 1, x % 3].colorbar()
			#axarr[x / 3 + 1, x % 3].set_title("Styple %3d, Part %1d" %(int(last[-5:]), label / 32))
			#potential_image = cv.resize(potential_hair, (227, 227), interpolation = cv.INTER_NEAREST)
			#print os.path.join(path, "strands00002_0120_mask.png")
			#draw_rectangle(potential_image, *bounding_box(potential_image, label))
			#highlight(potential_image, label, color = x * 50)
			#save_image(os.path.join(result_path, str(x) + "_label.png"), potential_image)
			#show_image(potential_image)	
		#show_image(image_tmp)
		save_image(os.path.join(result_path, "img_tmp.png"), image_tmp)
		if file_path != "":
			axarr[0, 0].imshow(bgr2rgb(cv.imread(file_path)))
		axarr[0, 0].set_title("Image")
		axarr[0, 1].imshow(bgr2rgb(self.image))
		axarr[0, 1].set_title("Hair Region(In center)")
		axarr[0, 2].imshow(bgr2rgb(image_tmp))
		axarr[0, 2].set_title("Clustering")
		#plt.show()
		plt.savefig(os.path.join(output, hair_name + "_result.jpg"), dpi = 1000)
		#plt.savefig()
		return result_path
		"""
		potential = unique

		potential_path = os.path.join(result_path, "potential")
		check_or_create(potential_path)
		for i in xrange(len(potential)):
			if potential[i] == 341:		# The label of background
				continue
			label = self.labelTransformer.caffe2real(potential[i])
			cnt = 113
			shutil.copy(os.path.join(self.data_path, self.parser.get_path(label), self.parser.get_hair(label, cnt)), 
						os.path.join(potential_path, str(i) + "_" + self.parser.get_hair(label, cnt)))
		"""
	# Draw a rectangle on image
	# Crop the tmp with the guide of self.image
	def crop_map(self, tmp):
		#analyse_image(self.mask)
		return tmp * self.mask + 0 * self.background

	
	def crop(self, tmp):
		return tmp * self.mask
	
	def blur_predict(self):

		#analyse_image(self.predict)
		#print self.predict
		for c in range(self.predict.shape[0]):
			#self.predict[c] = cv.blur(self.predict[c],(10,10))# - self.predict
			self.predict[c] = cv.GaussianBlur(self.predict[c],(9,9),0)
		#print self.predict
		#analyse_image(self.predict)
	# Use the cluster in clusters(a iterator), to refine the predict_map
	def refine_cluster(self, predict_map, clusters):
		n = self.predict.shape[1]
		m = self.predict.shape[2]
		#new_predict_map = predict_map.copy()#np.zeros((n, m))
		new_predict_map = np.zeros((n, m))
		new_predict = self.predict[clusters, :, :]
		new_predict_map = new_predict.argmax(axis = 0)
		analyse_image(new_predict_map)
		for x in range(len(clusters)):
			np.place(new_predict_map, new_predict_map == x, clusters[x])
		analyse_image(new_predict_map)
		"""
		for i in xrange(n):
			for j in xrange(m):
				if 
				best_cluster = predict_map[i][j]
				best_score = -12321232
				for c in clusters:
					if self.predict[c][i][j] > best_score:
						best_cluster = c
						best_score = self.predict[c][i][j]
				new_predict_map = best_cluster
		"""
		#show_image_plt(new_predict_map)
		return new_predict_map
				

	def process(self, hair_name):
		image = cv.imread(hair_name)
		#save_image("origin_" + hair_name, image)
		print image.shape
		#image = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
		
		u, l, d, r = bounding_box.bounding_box_nonzero(image)
		
		self.frame_size = 227
		image = bounding_box.fill_center_normalize(image[u:d, l:r], int(227 * 0.8), 227)
		#show_image(image)
		#image = cv.resize(image, (227, 227))
		return image
		#show_image(image)
def bgr2rgb(img):
	return cv.cvtColor(img, cv.COLOR_BGR2RGB)
def highlight_guided(img, img_guide, color_h):
	#show_image_plt(img_guide)
	img_hsv = cv.cvtColor(img, cv.COLOR_BGR2HSV)
	n = img.shape[0]
	m = img.shape[1]
	for i in xrange(n):
		for j in xrange(m):
			if img_guide[i][j]:
				pass
				#print i, j 
				#print img_hsv[i][j][0]
				img_hsv[i][j][0] = color_h
				img_hsv[i][j][1] = 120

	return cv.cvtColor(img_hsv, cv.COLOR_HSV2BGR)

		
def highlight(img, label, color = 0):
	rgb =  cv.cvtColor(np.uint8([[[color, 128, 128]]]), cv.COLOR_HSV2BGR)[0][0]
	rgb = tuple(rgb.tolist())
	x,y,z = rgb
	n = img.shape[0]
	m = img.shape[1]
	for i in xrange(n):
		for j in xrange(m):
			if img[i][j][0] == label:
				img[i][j][1] = 255
				img[i][j][2] = 0
				#img[i][j][0], img[i][j][1], img[i][j][2] = x,y,z

def highlight1(img, img1, label, color = 0):
	n = img.shape[0]
	m = img.shape[1]
	for i in xrange(n):
		for j in xrange(m):
			if img1[i][j] != label and img1[i][j] != 0:
				img[i][j][0] = 255
				img[i][j][1] = 255
				img[i][j][2] = 255

def draw_rectangle(img, l, r, u, d, color = 0):
	print l,r,u,d
	#show_image(img)
	rgb =  cv.cvtColor(np.uint8([[[color, 128, 128]]]), cv.COLOR_HSV2BGR)[0][0]
	rgb = tuple(rgb.tolist())
	x,y,z = rgb
	print rgb
	print x, y, z


	cv.rectangle(img, (l,u), (r,d), (x,y,z), thickness = 1)

#Show the predict for each heat map.

def show_predict(predict, candidate = []):
	#predict_sum = predict.sum(axis = (1,2))

	for x in candidate[:5]:
		plt.imshow(predict[x])
		plt.colorbar()
		plt.show()
	"""
	for x in xrange(predict.shape[0]):
		if predict[x].sum(axis = (0,1)) > 8000:
			show_image_plt(predict[x])
	"""

if __name__ == "__main__":
	#process("test.png")
	m = HairMapper(snapshot = experiment)
	
	m.mapping("test1.png", "")
