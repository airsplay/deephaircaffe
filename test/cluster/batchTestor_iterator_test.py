from hairMapper_test1 import *
from hairTools import *
import os
import shutil
import sys
import re
import liwen_data
experiment = "alexnet_finetune_cluster_hair_deconv_alexnet_55_no_pooling_hair_cluster_hair_front"
#experiment = "alexnet_finetune_mask_deconv_alexnet_227_no_pooling_hair_random"
#experiment = "alexnet_finetune_mask_deconv_alexnet_227_no_pooling_1fc_hair_random"
#experiment = "alexnet_finetune_mask_deconv_alexnet_27_no_pooling_lfw_image"
#experiment = "alexnet_finetune_mask_deconv_alexnet_55_no_pooling_mixedData7"
class Test:

	def __init__(self, snapshot = ""):
		self.c = HairMapper(snapshot = snapshot)
	
	#if_gray:0,not, 1:yes, 2:both
	# hair: if test on only hair region
	def batch_test(self, folder, hair = False):
		file_list = os.listdir(folder)
		os.chdir(folder)
		result_path = "../" + folder + "_" + experiment
		check_or_create(result_path)
		for f in file_list:
			if hair:		# Skip the non-hair images
				if not("hair" in f or "matting" in f or "aoriginal_frg" in f):
					continue
			#tmp_path = os.path.join(result_path, f)
			result_path_tmp = self.c.mapping(f, output = result_path)
			shutil.copy(f[:-9] + ".jpg", result_path_tmp)
		os.chdir("..")
			
	def batch_test_iterator(self):
		result_path = "./liwen_data_" + experiment + "_center_stride_30"
		for d in liwen_data.HairIterator(dict_form = True):
			self.c.mapping(os.path.join(d['path'], d['hair']), 
						os.path.join(d['path'], d['file']), 
						output = result_path)


t = Test(snapshot = experiment)
t.batch_test_iterator()
#for folder in ["data_yi", "selected", "MattingTan"]:
#for folder in ["selected"]:
	#t.batch_test(folder, hair = True)
			
			
		
		
	

