from analysis import *
from hairTools import *
import matplotlib.pyplot as plt
import shutil
import bounding_box
import re
import numpy as np
import matplotlib.pyplot as plt
import os
import sys
import cv2 as cv
from strider import Strider

#experiment = "alexnet_finetune_map_hair_deconv_alexnet_55_no_pooling_hair_random_hair"
experiment = "alexnet_finetune_cluster_hair_deconv_alexnet_55_no_pooling_hair_cluster_hair_front"
class HairMapper(Analysis):
	
	def __init__(self, *args, **kwargs):
		Analysis.__init__(self, *args, **kwargs)
		self.result_path_tmp = "%s.result"						# The template of result folder's name
		self.parser = HairParser()
		self.labelTransformer = HairLabelTransformer()			# Transfer the label
		self.data_path = "D:\\airsplay\\HairTrain\\data\\hair"	# For selecting the typical potential result
		self.net.blobs['data'].reshape(1,3,227,227)				# One data for a test

	# Load data from 
	# 1. File if data is a string
	# 2. Numpy.array if data is so
	def load_data(self, data):
		
		if isinstance(data, str):
			self.net.blobs['data'].data[...] = self.transformer.preprocess('data', caffe.io.load_image(data))
		else:
			print data.shape
			# Judge the image as gray-scale image or an RGB image
			if len(data.shape) > 2:
				self.net.blobs['data'].data[...] = data.transpose(2,0,1)	# The gray-scale image automatically fill all the channels
			else:
				self.net.blobs['data'].data[...] = data 

		# Start forwarding after loading the data
		self.net.forward(start = 'conv1')
	
	# Get the map of an particular image and put the result in output 
	def mapping(self, hair_path,  file_path = "", output = "."):
		hair_folder, hair_name = os.path.split(hair_path)	
		check_or_create(output)		# The output of result(basic result)
		
		self.image = self.process(hair_path)
		tmp = cv.resize(self.image, (55,55))
		tmp = tmp.sum(axis = 2)
		self.mask = tmp > 0 
		self.background = (tmp == 0)
		self.load_data(self.image)



		predict = self.net.blobs['out'].data[0]				# Still a C*W*H array while C is the probability vector before sigmoid
		self.predict = predict								# The origin array of predict

		stri = Strider(self.predict, self.mask)
		stri.stride()

	# Draw a rectangle on image
	# Crop the tmp with the guide of self.image
	def crop_map(self, tmp):
	
		return tmp * self.mask + 0 * self.background

	
	def crop(self, tmp):
		return tmp * self.mask
	
	def blur_predict(self):

		#analyse_image(self.predict)
		#print self.predict
		for c in range(self.predict.shape[0]):
			#self.predict[c] = cv.blur(self.predict[c],(10,10))# - self.predict
			self.predict[c] = cv.GaussianBlur(self.predict[c],(9,9),0)
		#print self.predict
		#analyse_image(self.predict)
	# Use the cluster in clusters(a iterator), to refine the predict_map
	def refine_cluster(self, predict_map, clusters):
		n = self.predict.shape[1]
		m = self.predict.shape[2]
		#new_predict_map = predict_map.copy()#np.zeros((n, m))
		new_predict_map = np.zeros((n, m))
		new_predict = self.predict[clusters, :, :]
		new_predict_map = new_predict.argmax(axis = 0)
		analyse_image(new_predict_map)
		for x in range(len(clusters)):
			np.place(new_predict_map, new_predict_map == x, clusters[x])
		analyse_image(new_predict_map)
		"""
		for i in xrange(n):
			for j in xrange(m):
				if 
				best_cluster = predict_map[i][j]
				best_score = -12321232
				for c in clusters:
					if self.predict[c][i][j] > best_score:
						best_cluster = c
						best_score = self.predict[c][i][j]
				new_predict_map = best_cluster
		"""
		#show_image_plt(new_predict_map)
		return new_predict_map
				

	def process(self, hair_name):
		image = cv.imread(hair_name)
		#save_image("origin_" + hair_name, image)
		print image.shape
		#image = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
		
		u, l, d, r = bounding_box.bounding_box_nonzero(image)
		
		self.frame_size = 227
		image = bounding_box.fill_center_normalize(image[u:d, l:r], int(227 * 0.8), 227)
		#show_image(image)
		#image = cv.resize(image, (227, 227))
		return image
		#show_image(image)
def bgr2rgb(img):
	return cv.cvtColor(img, cv.COLOR_BGR2RGB)
def highlight_guided(img, img_guide, color_h):
	#show_image_plt(img_guide)
	img_hsv = cv.cvtColor(img, cv.COLOR_BGR2HSV)
	n = img.shape[0]
	m = img.shape[1]
	for i in xrange(n):
		for j in xrange(m):
			if img_guide[i][j]:
				pass
				#print i, j 
				#print img_hsv[i][j][0]
				img_hsv[i][j][0] = color_h
				img_hsv[i][j][1] = 120

	return cv.cvtColor(img_hsv, cv.COLOR_HSV2BGR)

		
def highlight(img, label, color = 0):
	rgb =  cv.cvtColor(np.uint8([[[color, 128, 128]]]), cv.COLOR_HSV2BGR)[0][0]
	rgb = tuple(rgb.tolist())
	x,y,z = rgb
	n = img.shape[0]
	m = img.shape[1]
	for i in xrange(n):
		for j in xrange(m):
			if img[i][j][0] == label:
				img[i][j][1] = 255
				img[i][j][2] = 0
				#img[i][j][0], img[i][j][1], img[i][j][2] = x,y,z

def highlight1(img, img1, label, color = 0):
	n = img.shape[0]
	m = img.shape[1]
	for i in xrange(n):
		for j in xrange(m):
			if img1[i][j] != label and img1[i][j] != 0:
				img[i][j][0] = 255
				img[i][j][1] = 255
				img[i][j][2] = 255

def draw_rectangle(img, l, r, u, d, color = 0):
	print l,r,u,d
	#show_image(img)
	rgb =  cv.cvtColor(np.uint8([[[color, 128, 128]]]), cv.COLOR_HSV2BGR)[0][0]
	rgb = tuple(rgb.tolist())
	x,y,z = rgb
	print rgb
	print x, y, z


	cv.rectangle(img, (l,u), (r,d), (x,y,z), thickness = 1)

#Show the predict for each heat map.

def show_predict(predict, candidate = []):
	#predict_sum = predict.sum(axis = (1,2))

	for x in candidate[:5]:
		plt.imshow(predict[x])
		plt.colorbar()
		plt.show()
	"""
	for x in xrange(predict.shape[0]):
		if predict[x].sum(axis = (0,1)) > 8000:
			show_image_plt(predict[x])
	"""

if __name__ == "__main__":
	#process("test.png")
	m = HairMapper(snapshot = experiment)
	
	m.mapping("test1.png", "")
