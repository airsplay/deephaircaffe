from analysis import *
from hairTools import *
import shutil
import re
import numpy as np
import matplotlib.pyplot as plt
import os
import sys
import cv2 as cv

experiment = "alexnet_finetune_map_hair_deconv_alexnet_55_no_pooling_hair_random_hair"

class HairMapper(Analysis):
	
	def __init__(self, *args, **kwargs):
		Analysis.__init__(self, *args, **kwargs)
		self.result_path_tmp = "%s.result"						# The template of result folder's name
		self.parser = HairParser()
		self.labelTransformer = HairLabelTransformer()			# Transfer the label
		self.data_path = "D:\\airsplay\\HairTrain\\data\\hair"	# For selecting the typical potential result
		self.net.blobs['data'].reshape(1,3,227,227)				# One data for a test

	# Load data from 
	# 1. File if data is a string
	# 2. Numpy.array if data is so
	def load_data(self, data):
		
		if isinstance(data, str):
			self.net.blobs['data'].data[...] = self.transformer.preprocess('data', caffe.io.load_image(data))
		else:
			print data.shape
			# Judge the image as gray-scale image or an RGB image
			if len(data.shape) > 2:
				self.net.blobs['data'].data[...] = data.transpose(2,0,1)	# The gray-scale image automatically fill all the channels
			else:
				self.net.blobs['data'].data[...] = data 

		# Start forwarding after loading the data
		self.net.forward(start = 'conv1')
	
	# Get the map of an particular image and put the result in output 
	def mapping(self, file_name, output = "."):
		check_or_create(output)
		
		self.image = process(file_name)
		tmp = cv.resize(self.image, (55,55))
		tmp = tmp.sum(axis = 2)
		self.mask = tmp > 0 
		self.background = (tmp == 0)
		self.load_data(self.image)


		result_path = os.path.join(output, self.result_path_tmp % (file_name))
		check_or_create(result_path)

		self.save_input(result_path)
		self.save_conv_blobs(result_path)
		self.save_fc_blobs(result_path)
		print "Analysis compelete"
		#self.save_prob(result_path)

		predict = self.net.blobs['out'].data[0]				# Still a C*W*H array while C is the probability vector before sigmoid
		#print predict
		predict_map = convert_indicator_image(predict)		# Give a label to each pixel
		predict_prob = convert_probability_image(predict)	# Give a probability(confidential) to each pixel.

		save_image_plt(os.path.join(result_path, "predict_map.png"), predict_map)
		save_image_plt(os.path.join(result_path, "predict_prob.png"), predict_prob)

		predict_map = self.crop_map(predict_map)
		predict_prob = self.crop(predict_prob)

		save_image_plt(os.path.join(result_path, "hari_predict_map.png"), predict_map)
		save_image_plt(os.path.join(result_path, "predict_prob.png"), predict_prob)
		
		unique, counts = np.unique(predict_map, return_counts = True)

		pair = zip(unique, counts)
		print pair
		#pair = np.sort(pair, axis = 1)[::-1] # This Sort is WRONG!!!
		pair = sorted(pair, key = lambda x:x[1])[::-1]
		print pair
		unique, counts = zip(*pair)
		print unique, counts
		potential = unique

		potential_path = os.path.join(result_path, "potential")
		check_or_create(potential_path)
		for i in xrange(len(potential)):
			if potential[i] == 341:		# The label of background
				continue
			label = self.labelTransformer.caffe2real(potential[i])
			cnt = 113
			shutil.copy(os.path.join(self.data_path, self.parser.get_path(label), self.parser.get_file(label, cnt)), 
						os.path.join(potential_path, str(i) + "_" + self.parser.get_file(label, cnt)))
	# Crop the tmp with the guid of self.image
	def crop_map(self, tmp):
		analyse_image(self.mask)
		return tmp * self.mask + DEFAULT_LABEL * self.background
	def crop(self, tmp):
		return tmp * self.mask

def process(file_name):
	image = cv.imread(file_name)
	#save_image("origin_" + file_name, image)
	print image.shape
	#image = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
	image = cv.resize(image, (227, 227))
	return image
	#show_image(image)

if __name__ == "__main__":
	#process("test.png")
	m = HairMapper(snapshot = experiment)
	
	m.mapping("test.jpg")
