longX.aoriginal is the hair image by using the matting tool.
longX.aoriginal_prob shows the alpha chanel of longX.aoriginal
longX.aoriginal_mask is if(alpha>0) set 255,else set 0
long.colorsmoothMasked is the segmentation result using longX.aoriginal_prob as the probability map, color histogram and smooth term together.