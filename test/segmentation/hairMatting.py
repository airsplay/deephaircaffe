from analysis import *
from hairTools import *
import bounding_box
import shutil
import re
import numpy as np
import matplotlib.pyplot as plt
import os
import sys
import cv2 as cv
experiment = "alexnet_finetune_mask_deconv_alexnet_55_no_pooling_hair_random"
class HairMatting(Analysis):
	
	def __init__(self, *args, **kwargs):
		#print "init"
		Analysis.__init__(self, *args, **kwargs)
		self.result_path_tmp = "%s.matting"
		
		#print "Using prototxt %s \n and caffemodel %s" % args

	def load_data(self, data):
		
		if isinstance(data, str):
			self.net.blobs['data'].data[...] = self.transformer.preprocess('data', caffe.io.load_image(data))
		else:
			print data.shape
			if len(data.shape) > 2:
				self.net.blobs['data'].data[...] = data.transpose(2,0,1)
			else:
				self.net.blobs['data'].data[...] = data 
			#for channel in xrange(3):
				#self.net.blobs['data'].data[0][channel] = data

		self.net.forward(start = 'conv1')
	def matting(self, file_name):
		image = process(file_name)
		self.load_data(image)

		result_path = self.result_path_tmp % (file_name)

		if not(os.path.exists(result_path)):
			os.mkdir(result_path)

		self.save_raw_input(result_path)
		self.save_input(result_path)
		self.save_conv_blobs(result_path)
		#self.save_fc_blobs(result_path)

		mask = self.net.blobs['out'].data[0]
		#print mask
		n = int(mask.size ** 0.5)
		mask = mask.reshape(n, n)
		mask = sigmoid(mask)
		
		save_image(os.path.join(result_path, "mask.png"), mask)

		discrete_mask = np.round(mask)
		save_image(os.path.join(result_path, "discrete_mask.png"), discrete_mask)		

		discrete_mask_227 = cv.resize(discrete_mask, (227,227))	
		mat = np.minimum(discrete_mask_227*255, image)

		save_image(os.path.join(result_path, "matting.png"), mat)
	def matting_rgb(self, file_name, output = "./", gray_input = False, renew = False, analysis = True):
		result_path = self.result_path_tmp % (file_name)
		result_path = os.path.join(output, result_path)
		if not(os.path.exists(result_path)):
			os.mkdir(result_path)	
		else:
			if not(renew):
				return
		image = process(file_name, gray_input)
		self.load_data(image)



		#self.net.forward()


		mask = self.net.blobs['out'].data[0]
		n = int(mask.size ** 0.5)
		mask = mask.reshape(n, n)
		mask = sigmoid(mask)
		discrete_mask = np.round(mask)

		if analysis:
			self.save_raw_input(result_path)
			self.save_input(result_path)
			self.save_conv_blobs(result_path)
			save_image(os.path.join(result_path, "mask.png"), mask)
			save_image(os.path.join(result_path, "discrete_mask.png"), discrete_mask)	

		discrete_mask_227 = cv.resize(discrete_mask, (227,227))	
		if not(gray_input):
			discrete_mask_227 = cv.cvtColor(discrete_mask_227, cv.COLOR_GRAY2BGR)
		print discrete_mask_227.shape
		print image.shape
		mat = np.minimum(discrete_mask_227*255, image)
		if analysis:
			save_image(os.path.join(result_path, "matting.png"), mat)
		save_image(os.path.join(output, file_name + ".probability.png"), mask)
		save_image(os.path.join(output, file_name + ".matting.png"), mat)
		save_image(os.path.join(output, file_name), image)




def process(file_name, gray_input):
	image = cv.imread(file_name)
	#save_image("origin_" + file_name, image)
	print image.shape
	if gray_input:
		image = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
		
	pad_size = 0
	#image = cv.resize(image, (227 - pad_size * 2, 227 - pad_size * 2))
	image = bounding_box.image_adjust(image)
	#image = np.pad(image, ((pad_size, pad_size), (pad_size, pad_size)), 'constant')
	
	
	#show_image(image)
	#save_image(file_name, image)
	return image
	#print image
if __name__ == "__main__":
	#process("test.png")
	
	c = HairMatting(get_prototxt(experiment), get_caffemodel(experiment))
	c.save_feature_map()
	c.matting_rgb("orange2.jpg", gray_input = False)
