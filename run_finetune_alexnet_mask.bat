python setup.py -m alexnet_finetune_mask_deconv_alexnet_55_no_pooling --data hair_hair --gray hair_hair_gray --source lmdb --batch_size=128 --pseudo_pool_group=16
pause
python step_train.py --save 100 -s snapshot/alexnet_finetune_mask_deconv_alexnet_55_no_pooling_mixedData7/alexnet_finetune_mask_deconv_alexnet_55_no_pooling_mixedData7_iter_225000.solverstate
pause