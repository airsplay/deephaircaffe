#Models
==Only `solver.prototxt` and `train\_val\_db.prototxt` is useful. I didn't maintain any other files==

##Names
###Alexnet\_finetune\_*
The network based on alexnet
####alexnet\_finetune\_auto\_*
---------------------------
Autoencoder based on alexnet
#####alexnet\_finetune\_auto\_deconv
Fully end-to-end autoencoder.

####alexnet\_finetune\_map\_*
----------------------------
The models focus on solve map problem
#####alexnet\_finetune\_map\_hair\_*
Using segmented hair region as input.
==I found that it's important to decrease dropout ratio to get better result==
Since the bottlenect is already compact. Large dropout will not be useful and 

####alexnet\_finetune\_fixed\_classifier
Fixed the parameter in the encoder. Train the classifier of the last few layers.
It equals to a logistic regression on pretrained features

####alexnet\_finetune\_mask\_*
--------------------------
The Models focus on solve the segmentation problem
#####alexnet\_finetune\_mask\_deconv\_alexne\_55\_no\_pooling
1. Using deconvolution layer as decoder.
2. output and mask of 55 * 55.
3. Eliminate pooling layer by convolutoin layer with large gropu-num

#####alexnet\_finetune\_mask\_deconv\_VGG\_55
The decoder is based on VGG.

#####alexnet\_finetune\_mask\_google\_55
1. Using one google layer as filter.
2. decoder based on fully connection

###VGG\_finetune\_*
The network based on VGG. Not support well till now.

##Structure of Model
1. train\_val\_db.prototxt : The most important prototxt in each model.
2. solver.prototxt
3. output.jpg: The structure of each network.
