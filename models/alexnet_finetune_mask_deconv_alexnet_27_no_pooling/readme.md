#Alexnet finetune without pooling.
##Main changes
1. Using conv instead of pooling layer.
2. Delete the fully connection layer to avoid over fitting.

##Basic
1. Encoder: Alexnet
2. Decorder: VGG-like deconvolution