It's a guide to this project. 

Software and environment:
1. Caffe and its dependency
	1. Caffe 
		Caffe is a framework for deep learning, used widely in Vision.
		There are many other framework. 
		Theano: A python platform framework. Designed by Canadian Mafia. The famous deeplearning.net tutorial use Theano.
			It's widely used in RNN because RNN has many novel structure, many variants of LSTM in particular.
		Torch:	A framework use Lua script.
			I thought that it widely used in speech. It's friendly to RNN.
		Mxnet: Developed by DMLC. This group also wrote Kaggle Killer, XGboost.
		convnet: Developed by Hinton's group. This group won ImageNet 2012 and the  of deep learning came.
			But, they released their code too late, so most of us use caffe.
		TensorFlow: I have nothing to say.
	2. Platform and Version
		The Caffe on github is designed for Linux. You must do some surgeries on the code to transfer it to the windows platform.
		
		I upload the windows code and all its dependencies for convinience.
		This caffe program is compiled by Xiao Li (Principal Intern, God Xiao). He got all the things work well.
		I only MODIFY some code to support fully convolutional network. The newest version of CAFFE doesn't have this BUG.
		
		I can't promise that all the code will work well if you use the newest caffe. As we know, caffe is still a developing tool.
	3. Interface
		There are two mainstream interface in Caffe. Matlab and python.
		We used python in our group.
		By the way, our neighbour group -- VC, use Matlab. 
		
		Python is the best language in the world. It is powerful, flexible and robust. ( I ignore about 10000 words here ) 
		However, matlab could draw graph easily. It's enough.
	4. Dependencies
		1. Xiao Li has put most of the dependencies of Caffe in the folder "bin", please check it.
		2. You must install openCV by yourself and list the dll folder in your path.

2. Documentation
Most of the documentation are written in Markdown. The program that I used to show the markdown is Haroopad.

3. Python 2.7
		1. Please Install Anaconda 2.3.0. Anaconda use numpy 1.10.1+ since 2.4.0. You will find that many things didn't work correctly under numpy 1.10.1
		2. If you want to draw the net by the default caffe program. You should install graphviz and you need to change some code in pydot. You could search the error message in pydot.py and you need to explicitly write the path to Graphviz in pydot.
		3. Pythonpath
			You should add the "data" folder, "tools" folder and path to analysis.py in your pythonpath as well as the path to the caffe pylib.
		4. If you have any problem, use pydoc to find the result.
			ipython is also useful. Please check the tutorial.

Code format:
I used python to wrote most of the code.  I use TAB to indent instead of 4 spaces.
I used vim as my IDE and editor. I forget to kill the backup and swap options in .vimrc.
(You know that, I focus on the color ~)





Content of Folders:

First. DATA: 
Include all the data that I used.

1. The origin image are stored such folders:
hair: The origin hair data.
*hair_cluster: The hair image with cluster id.
*hair_cluster_hair: Add variation to the hair_cluster data and use the mask to generate only hair.
hair_cluster_hair_front: The same to hair_cluster_hair, but only use the front face.
hair_new: I asked Liwen to rerender the hair with long body. The data doesn't have unexpected hair.
hair_random:	Mixed the hair(in folder "hair") with other background.(In the folder "background" under hair_random). Randomly change the color and the position of the hair.
lfw: The origin lfw dataset. 

2. Other folders are just a place to allocate LMDB database.
LMDB is a key-value database which is the default database used in CAFFE. It speed up the training by 2X.
Their are two general kinds of LMDB generation. 
1. The convert_to_lmdb.bat.
	It call the C++ backend "convert_imageset" in Caffe/script ( You could find it in the caffe source code, not in my project). 
	The parameter of this script is self-explained. 
	convert_imageset "" trainInput --resize_height=227 --resize_width=227 --shuffle=true train_lmdb
	I used chinese to explain it.
	第二个参数是起始目录，我这么写表示根目录，表示第三个参数的文件中的文件地址是完整地址。
	第三个参数是 image list的文件。
	resize height /width 是你需要得到的新的图片。我没有看backend的写法，不过根据效果来看应该是bilinear插值。
	shuffle是说我对image list是否需要重新shuffle。
	You could find an example in the folder "hair_constant"
2. The python script written by myself which call the lmdb lib in python to create lmdb database.
	It is used to deal with some fantasy tasks such as using mask as the 4th channel of the data.
	A example is in the hair_random_file_and_mask, which use lmdb_new.py to generate a self-defined lmdb database.
	lmdb_show.py is used to check whether the lmdb is created correctly.
	To use(change) this script correctly, you should learn the format of DATA in caffe(or design your own data fetecher) and learn the interface of lmdb. The interface is easy and self-explained, you could find it in my code.

3. For each task, I used slightly different format of DATA. 
1. Classification. 
The basic task of NN. We useually put an image with a label in LMDB. They are conjugate as a value which only has one key. It could be read / cached correctly and efficiently by Datum in Caffe.
2. Mask/Segmentation/Cluster
We should give the NN two images as input. The first one is the origin image and the second one is the segmentation/Mask
There are two way to do it.
1. Use two data layers.
I am used to use two Data layers and created an lmdb for each. Caffe will load the data with same key from each data layer.
You must explicitly write two data layers in caffe prototxt.
An example is "models\alexnet_finetune_mask_deconv\train_val_db.prototxt". You could find an image there and there are two data layers named "data" ( The origin data ) and "mask"
However, I found that the only thing you need to do is add the image to LMDB which follow the same order.
An example is the pair of "hair_cluster" and "hair_cluster_front_mask_55"
2. Use RGBA channel and split in the model.
The representative model is "models\VGG_mask_deconvolution\train_val_db.prototxt". It use a slice layer to split the channels. 
The data is "hair_random_file_and_mask".
Although I hacked caffe to make everything correct, the best way to fetch the data is to write the data layer by yourself. It's a hard work.... If I have one year in MSRA, I could create it .





Second. Training
The training process is written in C++. The interface that I used is the python wrapper. 
I wrote some bat (for convenience, one bat file is enough) which call two scripts to accomplish the training process.
"setup.py" is script which modify the particular model and put it under models/. 
"step_train.py". Please read it. I annotated it carefully. In general, this script run the Solver (A differential optimizer generate by caffe kernel) one iteration each time.

For each training, there are two important things.
The first one is DATA which I have explained it before. I prefer LMDB
The second one is model. All the models I used are listed in the folder "model".
there are two files for each model.
1. "solver.prototxt". A file which define the optimizer.
Here is an example from model\alexnet_finetune_mask_deconv
net: "train_val.prototxt" 使用什么具体的model
#test_iter: 10						如果test的话，那么一次test跑多少个iteration
#test_interval: 500				如果test，那么隔多少个iteration test一次。如果两个都注释掉，则不进行test。
base_lr: 0.0000001				最开始的时候的Learning rate 
lr_policy: "step"					Learning rate有不同的变化方式，这里使用的是最经典的step方式，阶梯下降法。即过多长时间下降一次。
gamma: 0.1								每一次达到step size，就将learning rate乘以这个参数
stepsize: 200000					隔多长时间执行step操作
display: 1								隔多长时间display一次（显示learning rate，loss等，是系统定义的信息，与我的train-step脚本无关）
max_iter: 1000000					最多执行多少个iteration。（事实上要根据epoch来决定，epoch指将所有data过一遍）
momentum: 0.9							动量，这个比较悬，一般就0.9或者0.99。具体请查看SGD的运行方式。
weight_decay: 0.0005			每次weight会减小这么多，相当于L2regulization。可以设置成L1的。
snapshot: 2000						隔多长时间存储一次。
snapshot_prefix: "snapshot/modelName_dataName"		存储到哪里
solver_mode: GPU					使用什么硬件。
2. "train_val_db.prototxt". A file which define the structure of layered network.
Please check caffe\src\caffe\proto for the definition of each layer.

---------About caffe code-------------
You don't need to read the code of CAFFE, but I recommend you to read the caffe.proto. All the definitions of parameters are in this file. 
The parameter of layers are amazing and you need to ask other people.
Some parameter was not explained in caffe proto but you could find it in the code.
---------End--------------------------



Third. Test
