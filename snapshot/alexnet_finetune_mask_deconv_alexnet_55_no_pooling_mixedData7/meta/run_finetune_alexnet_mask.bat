python setup.py -m alexnet_finetune_mask_deconv_alexnet_55_no_pooling --data mixedData7 --mask mixedMask7 --source lmdb --batch_size=31 --pseudo_pool_group=16
pause
python step_train.py --save 100 -m snapshot/alexnet_finetune_mask_deconv_alexnet_55_no_pooling_mixedData10/alexnet_finetune_mask_deconv_alexnet_55_no_pooling_mixedData10_iter_3000.caffemodel 2>&1 | mtee train_log
pause