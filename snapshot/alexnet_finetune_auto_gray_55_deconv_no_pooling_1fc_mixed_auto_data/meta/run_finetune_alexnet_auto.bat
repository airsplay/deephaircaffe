python setup.py -m alexnet_finetune_auto_gray_55_deconv_no_pooling_1fc --data mixed_auto_data --gray mixed_auto_gray_55 --source lmdb --batch_size=96 --pseudo_pool_group=16
pause
python step_train.py --save 100 --auto -s snapshot/alexnet_finetune_auto_gray_55_deconv_no_pooling_1fc_mixed_auto_data/alexnet_finetune_auto_gray_55_deconv_no_pooling_1fc_mixed_auto_data_iter_4000.solverstate 2>&1 | mtee train_log
pause