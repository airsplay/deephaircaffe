python setup.py -m alexnet_finetune_mask_deconv --data hair_origin --mask hair_mask --source lmdb --batch_size=10
python step_train.py --save 100 --init_deconv -m snapshot/alexnet_finetune_mask_deconv_VGG_55_hair_origin/alexnet_finetune_mask_deconv_VGG_55_hair_origin_iter_68000.caffemodel 2>&1 | mtee train_log
pause