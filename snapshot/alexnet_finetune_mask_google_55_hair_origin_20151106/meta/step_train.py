
import caffe
import numpy as np
import cv2 as cv
import lmdb
import matplotlib.pyplot as plt

from pylab import *
from PIL import Image
import os
import getopt
from analysis import *
from hairTools import *


if __name__ == "__main__":
	ifshow = False
	save_iter = 0

	opts, args = getopt.getopt(sys.argv[1:], "m:s:", ["model=", "snapshot=", "multiGPU", "show", "save="])
	
	model = ""
	snapshot = ""
	for op, value in opts:
		if op == "-s" or op == "--snapshot":
			snapshot = value
		if op == "-m" or op == "--model":
			model = value
		if op == "--show":
			ifshow = True
		if op == "--save":
			save_iter = int(value)
	
	
	
	print "finish loading libraries"

	modelPath = "models"
	snapshotPath = "snapshot"

	if False:
		caffe.set_device(0)
		caffe.set_device(1)
		caffe.set_device_count(2)
	caffe.set_mode_gpu()

	solver = caffe.SGDSolver(os.path.join(modelPath, 'solver.prototxt'))
	if snapshot != "":
		solver.restore(os.path.join(snapshotPath, snapshot))
	if model != "":
		solver.net.copy_from(model)

	#solver.solve()
	#caffe.solve_multiGPU(solver)
	cnt = 0
	while True:
		solver.step(1)
		if ifshow or (save_iter > 0 and (cnt % save_iter == 0)):
			data = solver.net.blobs['data'].data[0]
			mask = solver.net.blobs['mask'].data[0][0]
			data = data.transpose(1,2,0)
			#mask = cv.resize(mask, (64, 64))
			#mask = mask * 255
			#mask.astype(np.uint8)
			predict = solver.net.blobs['out'].data[0]
			total = predict.shape[0]
			n = int(total ** 0.5)
			predict = sigmoid(predict).reshape(n,n)
			#predict = cv.resize(predict, (64, 64))
			if ifshow:
				show_image(data / 255)
				show_image(mask)
				show_image(predict)
			if (save_iter > 0):
				save_image("tmp/%06ddata.png" % (cnt), data/255)
				save_image("tmp/%06dmask.png" % (cnt), mask)
				save_image("tmp/%06dpredict.png" % (cnt), predict)
		cnt += 1
			
