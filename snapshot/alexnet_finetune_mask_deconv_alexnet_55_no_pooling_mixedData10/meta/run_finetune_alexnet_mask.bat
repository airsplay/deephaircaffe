python setup.py -m alexnet_finetune_mask_deconv_alexnet_55_no_pooling --data mixedData10 --mask mixedMask10 --source lmdb --batch_size=30 --pseudo_pool_group=16
pause
python step_train.py --save 100 -m snapshot/alexnet_finetune_mask_deconv_alexnet_55_no_pooling_hair_random/alexnet_finetune_mask_deconv_alexnet_55_no_pooling_hair_random_iter_28000.caffemodel 2>&1 | mtee train_log
pause