python setup.py -m alexnet_finetune_mask_deconv_alexnet_227_no_pooling_1fc --data hair_random --mask hair_random_mask --source lmdb --batch_size=50 --pseudo_pool_group=16
pause
python step_train.py --save 100 -m snapshot/alexnet_finetune_mask_deconv_VGG_27_no_pooling_hair_random/alexnet_finetune_mask_deconv_VGG_27_no_pooling_hair_random_iter_56000.caffemodel 2>&1 | mtee train_log
pause