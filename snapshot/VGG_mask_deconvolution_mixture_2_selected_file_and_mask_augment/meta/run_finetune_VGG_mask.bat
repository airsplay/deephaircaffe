python setup.py -m VGG_mask_deconvolution --data selected_file_and_mask_augment --source lmdb --batch_size=64 pseudo_pool_group=16

python step_train.py --seg --save 100 -m models/VGG_finetune/VGG_ILSVRC_16_layers.caffemodel 2>&1 | mtee train_log

pause