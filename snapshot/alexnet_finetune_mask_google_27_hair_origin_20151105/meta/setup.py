#Generate correct trainInput and testInput for training
#Generate proto in model

# The default configuration
#trainInputFileName = "trainInput"		# The file name of trainAndTest
#testInputFileName = "testInput"
import os
batch_size = 5
data_name = ""
mask_name = ""
modelName = ""
modelPath = "models"

data_path = "data"
data_path = os.path.abspath(data_path)

import getopt
import sys
import caffe
import caffe_pb2
import google.protobuf

def usage():
	print " -m nameOfModel " 
	print " -h #get help info "

class Setup:
	def __init__(self, modelName, data_name, using_db, classify):
		self.modelName = modelName
		self.data_name = data_name
		self.using_db = using_db
		self.classify = classify

	def load_solver(self): # Load the solver from solver.prototxt
		solver_parameter = caffe_pb2.SolverParameter()

		f = open(os.path.join(modelPath, modelName, "solver.prototxt"), 'r')
		if not f:
			pass
			return

		google.protobuf.text_format.Merge(f.read(), solver_parameter)
		f.close()

		self.net_file = solver_parameter.net
		self.solver_parameter = solver_parameter
	
	def modify_solver(self):	# Set the correct configuration for solver
		self.solver_parameter.net = os.path.join(modelPath, "train_val.prototxt")

		snapshotDir = os.path.join("snapshot", modelName + "_" + data_name)
		if not(os.path.exists(snapshotDir)):
			os.mkdir(snapshotDir)
		self.solver_parameter.snapshot_prefix = os.path.join(snapshotDir, modelName + "_" + data_name)
	
	def dump_solver(self):
		print "Dump the SOLVER to: " + os.path.join(modelPath, "solver.prototxt")
		s = google.protobuf.text_format.MessageToString(self.solver_parameter)
		f = open(os.path.join(modelPath, "solver.prototxt"), 'w')
		f.write(s)
		f.close()

	def load_net(self): # Load the net parameter from the configuration in solver.prototxt
		self.net_parameter = caffe_pb2.NetParameter()
		if self.using_db == "":
			print "Using db format: None"
			f = open(os.path.join(modelPath, modelName, self.net_file), "r")
		elif self.using_db == "lmdb":
			print "Using db format: lmdb"
			f = open(os.path.join(modelPath, modelName, "train_val_db.prototxt"), "r")
		elif self.using_db == "leveldb":
			print "Using db format: leveldb"
			f = open(os.path.join(modelPath, modelName, "train_val_db.prototxt"), "r")
		else:
			print "Unknown DB " + self.using_db + ". Please check it"
		google.protobuf.text_format.Merge(f.read(), self.net_parameter)
		f.close()

	def load_tags(self): # Load the tags from data
		#f = open(os.path.join(data_path, data_name, "trainInput")
		#self.tags = 10 # for Mnist Data
		self.tags = 341 # for hair Data

	def modify_net(self):
		# Correct the configuration for data
		if self.using_db == "":
			for l in self.net_parameter.layer:
				if l.type == "ImageData":
					if l.name == "data":
						source_name = data_name
					elif l.name == "mask":
						source_name = mask_name
					# Set the batch size
					l.image_data_param.batch_size = batch_size
					# Set the source of data
					if l.include[0].phase == caffe_pb2.TEST:		# Set the test input
						testPath = os.path.join(data_path, source_name, "testInput")
						if not(os.path.exists(testPath)):
							print "Error: The file testInput of path " + testPath + " doesn't exist."
						l.image_data_param.source = testPath
					elif l.include[0].phase == caffe_pb2.TRAIN:	# Set the train input
						trainPath = os.path.join(data_path, source_name, "trainInput")
						if not(os.path.exists(trainPath)):
							print "Error: The file trainInput of path " + trainPath + " doesn't exist."
						l.image_data_param.source = trainPath
					else:
						print "Unknown phase, please set it as test or train"
		elif self.using_db in ["lmdb", "leveldb"]:
			for l in self.net_parameter.layer:
				if l.type == "Data":
					if l.name == "data":
						source_name = data_name
					elif l.name == "mask":
						source_name = mask_name
					#Set the batchSize
					l.data_param.batch_size = batch_size
					# Set the source of data
					if l.include[0].phase == caffe_pb2.TEST:		# Set the test input
						testPath = os.path.join(data_path, source_name, "test_" + self.using_db)
						if not(os.path.exists(testPath)):
							print "Error: The file " + testPath + " doesn't exist."
						l.data_param.source = testPath
					elif l.include[0].phase == caffe_pb2.TRAIN:	# Set the train input
						trainPath = os.path.join(data_path, source_name, "train_" + self.using_db)
						if not(os.path.exists(trainPath)):
							print "Error: The file " + trainPath + " doesn't exist."
						l.data_param.source = trainPath
					else:
						print "Unknown phase, please set it as test or train"
					# Set the backend type of source
					if self.using_db == "lmdb":
						l.data_param.backend = caffe_pb2.DataParameter.LMDB
					elif self.using_db == "leveldb":
						l.data_param.backend = caffe_pb2.DataParameter.LEVELDB

		if self.classify:
			self.load_tags()
			# Correct the configuration for the fully-connection classifier layer. A tricky way.
			for l in self.net_parameter.layer[::-1]:
				if l.type == "InnerProduct":
					l.inner_product_param.num_output = self.tags
					break

	def dump_net(self):
		print "Dump the NET to: " + os.path.join(modelPath, "train_val.prototxt")
		s = google.protobuf.text_format.MessageToString(self.net_parameter)
		f = open(os.path.join(modelPath, "train_val.prototxt"), 'w')
		f.write(s)
		f.close()
		
if __name__ == "__main__":
	classify = False
	opts, args = getopt.getopt(sys.argv[1:], "m:d:s:", ["mask=", "model=", "data=", "source=", "--classify"])
	
	using_db = ""
	for op, value in opts:
		if op == "-m" or op == "--model":
			modelName = value
		if op == "-d" or op == "--data":
			data_name = value
		if op == "-s" or op == "--source":
			using_db = value
		if op == "--mask":
			mask_name = value
		if op == "--classify":
			classify = True
		
	if not(os.path.exists(os.path.join(modelPath, modelName))):
		print "The model " + modelName + " doesn't exist"
	if not(os.path.exists(os.path.join(data_path, data_name))):
		print "The data " + data_name + " doesn't exist"
	
	print "Setup the network"
	print "Model: " + modelName
	print "Data: " + data_name
		
	setup = Setup(modelName, data_name, using_db, classify = classify)

	# Load and dump solver.prototxt
	setup.load_solver()
	setup.modify_solver()
	setup.dump_solver()

	# Load and dump train_val.prototxt
	setup.load_net()
	setup.modify_net()
	setup.dump_net()

