python setup.py -m alexnet_finetune_fixed_classifier --classify -d hair_hair -s lmdb --batch_size=128 --pseudo_pool_group=16
pause
python train.py -m snapshot/alexnet_finetune_auto_gray_55_deconv_no_pooling_1fc_mixed_auto_data/alexnet_finetune_auto_gray_55_deconv_no_pooling_1fc_mixed_auto_data_iter_6000.caffemodel  2>&1 | mtee train_log

pause
