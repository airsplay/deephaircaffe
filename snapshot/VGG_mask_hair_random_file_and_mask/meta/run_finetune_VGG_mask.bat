python setup.py -m VGG_mask --data hair_random_file_and_mask --source lmdb --batch_size=20 ::--pseudo_pool_group=16
pause
python step_train.py --seg --save 100 -m models/VGG_finetune/VGG_ILSVRC_16_layers.caffemodel 2>&1 | mtee train_log
pause