
import caffe
import numpy as np
import lmdb
import matplotlib.pyplot as plt

from pylab import *
from PIL import Image
import os
import getopt


if __name__ == "__main__":
	opts, args = getopt.getopt(sys.argv[1:], "m:s:", ["model=", "snapshot=", "multiGPU"])
	
	model = ""
	snapshot = ""
	for op, value in opts:
		if op == "-s" or op == "--snapshot":
			snapshot = value
		if op == "-m" or op == "--model":
			model = value
	
	
	
	print "finish loading libraries"

	modelPath = "models"
	snapshotPath = "snapshot"

	if False:
		caffe.set_device(0)
		caffe.set_device(1)
		caffe.set_device_count(2)
	caffe.set_mode_gpu()

	solver = caffe.SGDSolver(os.path.join(modelPath, 'solver.prototxt'))
	if snapshot != "":
		print snapshot
		solver.restore(snapshot)
	if model != "":
		print model
		solver.net.copy_from(model)

	solver.solve()
	#caffe.solve_multiGPU(solver)
