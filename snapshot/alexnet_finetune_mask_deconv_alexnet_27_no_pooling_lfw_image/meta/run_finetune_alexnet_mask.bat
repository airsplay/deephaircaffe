python setup.py -m alexnet_finetune_mask_deconv_alexnet_27_no_pooling --data lfw_image --mask lfw_mask --source lmdb --batch_size=50 --pseudo_pool_group=16
pause
python step_train.py --save 100 -m models/alexnet_finetune/bvlc_alexnet.caffemodel 2>&1 | mtee train_log
pause