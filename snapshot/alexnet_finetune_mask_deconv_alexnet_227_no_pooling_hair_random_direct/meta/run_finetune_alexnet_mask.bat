python setup.py -m alexnet_finetune_mask_deconv_alexnet_227_no_pooling --data hair_random --mask hair_random_mask --source lmdb --batch_size=15 --pseudo_pool_group=16
pause
python step_train.py --save 100 -m models/alexnet_finetune/bvlc_alexnet.caffemodel 2>&1 | mtee train_log
pause