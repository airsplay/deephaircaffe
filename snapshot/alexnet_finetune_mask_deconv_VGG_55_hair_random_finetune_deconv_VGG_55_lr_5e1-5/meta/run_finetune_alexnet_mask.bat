python setup.py -m alexnet_finetune_mask_deconv_VGG_55 --data hair_random --mask hair_random_mask_55 --source lmdb --batch_size=20
python step_train.py --save 100 -s snapshot/alexnet_finetune_mask_deconv_VGG_55_hair_random/alexnet_finetune_mask_deconv_VGG_55_hair_random_iter_8000.solverstate 2>&1 | mtee train_log
pause