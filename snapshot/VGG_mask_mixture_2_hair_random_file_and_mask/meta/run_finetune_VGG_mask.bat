python setup.py -m VGG_mask_mixture_2 --data hair_random_file_and_mask --source lmdb --batch_size=20 ::--pseudo_pool_group=16
pause
python step_train.py --seg --save 100 -m snapshot/VGG_mask_mixture_2_hair_random_file_and_mask/VGG_mask_mixture_2_hair_random_file_and_mask_iter_4000.caffemodel 2>&1 | mtee train_log
pause