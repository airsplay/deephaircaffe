:: Run hair
python setup.py -m alexnet_finetune_map_hair_deconv_alexnet_55_no_pooling --data hair_random_hair --map hair_random_map_55 --source lmdb --batch_size=45 --pseudo_pool_group=16
pause
python step_train.py --map --save 100 -s snapshot/alexnet_finetune_map_hair_deconv_alexnet_55_no_pooling_hair_random_hair/alexnet_finetune_map_hair_deconv_alexnet_55_no_pooling_hair_random_hair_iter_9000.solverstate 2>&1 | mtee train_log
pause
