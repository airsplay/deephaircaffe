
import caffe
import numpy as np
import cv2 as cv
import lmdb
import matplotlib.pyplot as plt

from pylab import *
from PIL import Image
import os
import getopt
from analysis import *
from hairTools import *


if __name__ == "__main__":
	ifshow = False
	if_init_deconv = False
	seg = False
	auto = False
	if_map = False
	save_iter = 0
	show_iter = 0

	opts, args = getopt.getopt(sys.argv[1:], "m:s:", ["model=", "snapshot=", "multiGPU", "show=", "save=", "init_deconv", "seg", "auto", "map"])
	
	model = ""
	snapshot = ""
	for op, value in opts:
		if op == "-s" or op == "--snapshot":
			snapshot = value
		if op == "-m" or op == "--model":
			model = value
		if op == "--show":
			ifshow = True
			show_iter = int(value)
		if op == "--save":
			save_iter = int(value)
		if op == "--init_deconv":
			if_init_deconv = True
		if op == "--seg":
			seg = True
		if op == "--auto":
			auto = True
		if op == "--map":
			if_map = True
	
	
	print "finish loading libraries"

	modelPath = "models"
	snapshotPath = "snapshot"

	if False:
		caffe.set_device(0)
		caffe.set_device(1)
		caffe.set_device_count(2)
	caffe.set_mode_gpu()

	solver = caffe.SGDSolver(os.path.join(modelPath, 'solver.prototxt'))
	if snapshot != "":
		solver.restore(snapshot)
	if model != "":
		solver.net.copy_from(model)
		if if_init_deconv:	# Set the deconv parameter to the conv setting
			filter_conv1 = solver.net.params['conv1'][0].data.copy()
			fb = filter_conv1[:, 0]
			fg = filter_conv1[:, 1]
			fr = filter_conv1[:, 2]
			fgray = fr * 0.299 + fg * 0.587 + fb * 0.114
			fgray = fgray[:, np.newaxis, ...] * 100
			solver.net.params['deconv7'][0].data[...] = fgray
			solver.net.params['deconv6'][0].data[...] = solver.net.params['conv2'][0].data.copy()

	a = Analysis(net = solver.net)
	#solver.solve()
	#caffe.solve_multiGPU(solver)
	cnt = 0
	while True:
		solver.step(1)
		#m = solver.net.blobs['map'].data[0][0]
		#analyse_image(m)
		if (show_iter > 0 and (cnt % show_iter == 0)) or (save_iter > 0 and (cnt % save_iter == 0)):
			data = solver.net.blobs['data'].data[0]
			#analyse_image(data, "data")
			data = data.transpose(1,2,0)
		

			if seg:
				mask = solver.net.blobs['mask'].data[0][0]
			if auto:
				gray = solver.net.blobs['gray'].data[0][0]


			if seg or auto:
				predict = solver.net.blobs['out'].data[0][0]
			if seg:
				predict = sigmoid(predict)
				discrete_predict = np.round(predict)
			#predict = cv.resize(predict, (64, 64))
		
			if (show_iter > 0 and (cnt % show_iter == 0)):
				a.save_conv_blobs("tmp")
				a.save_conv_filter("tmp")
				if if_init_deconv:
					a.save_deconv_filter("tmp")
				show_image(data / 255)
				show_image(mask)
				show_image(predict)
			if (save_iter > 0 and (cnt % save_iter == 0)):

				if cnt % (save_iter * 10) == 0: # For every save_iter * 10 Iterations, save the result into an seperate folder.
					check_or_create("tmp/%06d" % (cnt))
					a.save_conv_blobs("tmp/%06d" % (cnt))
				a.save_conv_blobs("tmp")

				if if_init_deconv:
					a.save_deconv_filter("tmp")

				if auto or seg or if_map:	
					
					save_image("tmp/%06ddata.png" % (cnt), data/255)
				if auto or seg:
					save_image("tmp/%06dpredict.png" % (cnt), predict)
				if auto:
					save_image("tmp/%06dgray.png" % (cnt), gray)
				if seg:
					save_image("tmp/%06dmask.png" % (cnt), mask)
					save_image("tmp/%06ddiscrete_predict.png" % (cnt), discrete_predict)
				if if_map:
					predict_map = solver.net.blobs['out'].data[0]
					g = solver.net.blobs['map'].data[0][0]
					predict_map = convert_indicator_image(predict_map)
					save_image_plt("tmp/%06dpredict_map.png" %(cnt), predict_map)
					#analyse_image(g, 'map')
					save_image_plt("tmp/%06dgroundtruth.png" %(cnt), g)
				#save_image("tmp/%06dpredict.png" % (cnt), predict.transpose(1,2,0))
		cnt += 1
			
