import numpy as np
import lmdb
import caffe
from hairTools import *

env = lmdb.open('train_lmdb', readonly = True)

with env.begin() as txn:
	cursor = txn.cursor()
	for k, v in cursor:
		print k 
		datum = caffe.proto.caffe_pb2.Datum()
		datum.ParseFromString(v)
		print datum.channels, datum.height, datum.width#, datum.float_data
		flat_x = np.fromstring(datum.data, dtype=np.uint8)
		x = flat_x.reshape(datum.channels, datum.height, datum.width)
		y = datum.label
		show_image(x.transpose(1,2,0))
