import os
import sys
import re
import analysis



dataPath = "D:\\airsplay\\HairTrain\\data\\hair_cluster_hair\\cnn00"
dataDirs = os.listdir(dataPath)

class HairIterator:
	def __init__(self):
		self.parser = analysis.HairParser()	

	def __iter__(self):
	# Convert this class to an Iterator
		return self.generator()

	def generator(self):
	# The generator to the files
		for name in filter(lambda x:"strands" in x and os.path.isdir(os.path.join(dataPath, x)), dataDirs): # Filter the folders
			matchPatch = re.match(r"strands(\d+)", name)	#Parse the folder name
			tag = int(matchPatch.group(1))					#Use XXXX of stransXXXX as tag

			files = os.listdir(os.path.join(dataPath, name))
			
			for f in files:
				if self.parser.match(f):
					yield (os.path.join(dataPath, name), f)

# Here is an example
if __name__ == "__main__":
	for path, f in HairIterator():
		print path, f
