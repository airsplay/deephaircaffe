import numpy as np
import lmdb
import caffe
import cv2 as cv
import os
import random
from hairIterator import *
from analysis import *
import hair_cluster

#Get the files for hair_cluster
#print "fuck"
parser = HairParser()
all_files = [(path, file_name) for path, file_name in HairIterator()]
random.shuffle(all_files)

hair_list = [os.path.join(path, name + "_hair.png") for path, name in all_files]

mask_list = [os.path.join(path, name + "_mask.png") for path, name in all_files]
f = open("train_input", "w")
for path, file_name in all_files:
	f.write(path + " " + file_name + "\n")

f = open("hair_train_input", 'w')
for i in hair_list:
	f.write(i + "\n")
	
f = open("mask_train_input", 'w')
for i in mask_list:
	f.write(i + "\n")


#exit()
map_size = len(all_files) * 227*227*3 * 4

print "The map_size", map_size
print "The total files are", len(all_files)

#Create lmdb
env = lmdb.open("train_lmdb", map_size = map_size)

cnt = 0 
l = 227
with env.begin(write=True) as txn:
	for path in hair_list:
		#print path
		if cnt % 1000 == 0:
			print cnt

		img = cv.imread(path)
		#show_image(img)
		#print (l, l)
		img = cv.resize(img, (l, l))

		datum = caffe.proto.caffe_pb2.Datum()
		datum.channels = img.shape[2]
		datum.height = l
		datum.width = l 

		img = img.transpose(2, 0, 1)
			
		#datum.float_data.extend(img.reshape(img.size).tolist())
		datum.data = img.tobytes()

		str_id = '{:08}'.format(cnt)
		txn.put(str_id.encode('ascii'), datum.SerializeToString())

		cnt = cnt + 1
		#if cnt == 10:
			#break
print cnt

cnt = 0
l = 55
map_size = len(mask_list) * l * l * 8
env = lmdb.open("../hair_cluster_mask_55/train_lmdb", map_size = map_size)
lt = hair_cluster.LabelTransfer()
with env.begin(write=True) as txn:
	for path, name in all_files:
		#print path
		if cnt % 1000 == 0:
			print cnt
		label, num = parser.parse(name)
		print label, num

		
		#img = cv.imread(path)
		img = hair_cluster.open_cluster_mask(os.path.join(path, name + "_mask.png"))
			
		print type(img)
		analyse_image(img)
		#show_image_plt(img)
		#show_image(img)
		#print (l, l)
		img = img / 32
		img = cv.resize(img, (l, l), interpolation = cv.INTER_NEAREST)
		#show_image_plt(img)
		img = img.astype(np.uint16)
		for i in xrange(l):
			for j in xrange(l):
				if img[i][j] == 0:
					img[i][j] = lt.get_ignore_label()
				else:
					img[i][j] = lt.part2unicode(label, img[i][j])
		analyse_image(img)

		datum = caffe.proto.caffe_pb2.Datum()
		datum.channels = 1
		datum.height = l
		datum.width = l 

		#img = img.transpose(2, 0, 1)
			
		datum.float_data.extend(img.reshape(img.size).tolist()) # shrunk the array to a list and save it
		#datum.data = img.tobytes()

		str_id = '{:08}'.format(cnt)
		txn.put(str_id.encode('ascii'), datum.SerializeToString())

		cnt = cnt + 1
		#if cnt == 10:
			#break
print cnt
