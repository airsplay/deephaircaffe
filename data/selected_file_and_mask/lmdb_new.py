import numpy as np
import lmdb
import caffe
import cv2 as cv
import os
import random
from analysis import *
from lfw.selected import HairIterator

#Get the files for hair_cluster
parser = HairParser()
all_files = [x for x in HairIterator(dict_form = True)]
random.shuffle(all_files)

f = open("train_input", "w")
for x in all_files:
	f.write(x['path'] + " " + x['file'] + "\n")


map_size = len(all_files) * 400*400* 4 * 8

print "The map_size", map_size
print "The total files are", len(all_files)

#exit()
#Create lmdb
env = lmdb.open("train_lmdb", map_size = map_size)

cnt = 0 
with env.begin(write=True) as txn:
	for i in xrange(20):
		for d in all_files:
			#l = random.randint(227, 256) # random get size from 227 to 256
			#l = 250 # The origin size of LFW
			#print path
			l = random.randint(227, 224 * 1.5)
			if cnt % 10 == 0:
				print cnt
				#txn.commit()

			file_name = os.path.join(d['path'], d['file'])
			hair_name = os.path.join(d['path'], d['hair'])

			file_img = cv.resize(cv.imread(file_name), (l, l))
			hair_img = cv.resize(cv.imread(hair_name, 0), (l,l), interpolation = cv.INTER_NEAREST)
			"""
			file_img = cv.imread(file_name)
			hair_img = cv.imread(hair_name)
			"""
			mask_img = hair_img > 0
			mask_img = mask_img.astype(np.uint8)
			
			analyse_image(mask_img)

			show_image(file_img)
			show_image(mask_img * 255)

			#show_image(hair)
			#analyse_image(mask)
			#show_image(mask * 255)

			file_img = file_img.transpose(2, 0, 1)
			mask_img = mask_img[np.newaxis, :]
			img = np.concatenate((file_img, mask_img), axis = 0)

			#print img.shape

			datum = caffe.proto.caffe_pb2.Datum()
			datum.channels = img.shape[0]
			datum.height = img.shape[1]
			datum.width = img.shape[2]
			datum.data = img.tobytes()

			str_id = '{:08}'.format(cnt)
			txn.put(str_id.encode('ascii'), datum.SerializeToString())

		cnt = cnt + 1
print cnt
