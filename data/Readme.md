#DATA
##Names
###hair\_*
The basic data without variations.
####hair\_hair\_random
The hair data with random select test data. (not sequantial)
###hair\_random\_*
The data based on the variations provided by Liwen


####hair\_random\_hair\_hair
The hair data based on data set with random variation.
Select random index as well.
###lfw\_*
LFW database
###mixed\_*
mixture of real data from ==LFW== and ==hair_random==

##Structure
1. A txt file which contains all the data files.
2. Some python scripts
3. A batch file to convert the data to lmdb