import math
import random

def cover_num(n): # See coupon collector
	k = 3
	return int(n * math.log(n) + k * n)

out = open("trainInput", 'w')
data = map(lambda x:open(x).readlines(), ["dataInput", "selectedHair", "realHair"])
rate = [7, 2, 1]
cover = map(lambda x: cover_num(len(x)), data)
total = int(max([1.0 * cover[i] / rate[i] * sum(rate) for i in xrange(1, len(cover))]))

sample_space = reduce(lambda x, y: x + [y] * rate[y], range(len(cover)), [])
print sample_space
for x in xrange(total):
	a = random.choice(sample_space)
	now = random.choice(data[a])
	out.write(now)
	

print "total samples are", total
