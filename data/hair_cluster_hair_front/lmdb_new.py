import numpy as np
import lmdb
import caffe
import cv2 as cv
import os
import random
from hairIterator import *
from analysis import *
import hair_cluster
from tqdm import tqdm

# Check all of the list of file exist
def check_file_list_exist(l):
	for file_name in tqdm(l):
		if os.path.exists(file_name):
			continue
		else:
			print "The file " + file_name + " not exists!!"

#Get the files for hair_cluster from the front view

#Oh no !!!! The name of the file is in a mess
parser = HairParser()
all_files = [(path, file_name) for path, file_name in HairIterator()]
#print all_files
random.shuffle(all_files)

hair_list = [os.path.join(path, name[:-4] + "_hair.png_hair.png") for path, name in all_files]

mask_list = [os.path.join(path, name[:-4] + "_mask.png_mask.png") for path, name in all_files]

#map(check_file_list_exist, [hair_list, mask_list])

f = open("train_input", "w")
for path, file_name in all_files:
	f.write(path + " " + file_name + "\n")

f = open("hair_train_input", 'w')
for i in hair_list:
	f.write(i + "\n")
	
f = open("mask_train_input", 'w')
for i in mask_list:
	f.write(i + "\n")


map_size = len(all_files) * 227*227*3 * 4

print "The map_size", map_size
print "The total files are", len(all_files)

#Create lmdb for hair
env = lmdb.open("train_lmdb", map_size = map_size)

cnt = 0 
l = 227
with env.begin(write=True) as txn:
	print "Start to write lmdb for hair:"
	for path in tqdm(hair_list):
		#print path
		if cnt % 1000 == 0:
			pass#print cnt

		img = cv.imread(path)
		#show_image(img)
		#print (l, l)
		img = cv.resize(img, (l, l))

		datum = caffe.proto.caffe_pb2.Datum()
		datum.channels = img.shape[2]
		datum.height = l
		datum.width = l 

		img = img.transpose(2, 0, 1)
			
		#datum.float_data.extend(img.reshape(img.size).tolist())
		datum.data = img.tobytes()

		str_id = '{:08}'.format(cnt)
		txn.put(str_id.encode('ascii'), datum.SerializeToString())

		cnt = cnt + 1
		#if cnt == 10:
			#break
print cnt

# Create lmdb for mask
cnt = 0
l = 55
map_size = len(mask_list) * l * l * 8
env = lmdb.open("../hair_cluster_front_mask_55/train_lmdb", map_size = map_size)
lt = hair_cluster.LabelTransfer()
with env.begin(write=True) as txn:
	print "Start to write lmdb for mask:"
	for path, name in tqdm(all_files):
		#print path
		if cnt % 1000 == 0:
			pass#print cnt
		label, num = parser.parse(name)
		#print label, num

		
		#img = cv.imread(path)
		img = hair_cluster.open_cluster_mask(os.path.join(path, name[:-4] + "_mask.png_mask.png"))
			
		#print type(img)
		#analyse_image(img)
		#show_image_plt(img)
		#show_image(img)
		#print (l, l)
		img = img / 32
		img = cv.resize(img, (l, l), interpolation = cv.INTER_NEAREST)
		#show_image_plt(img)
		img = img.astype(np.uint16)
		for i in xrange(l):
			for j in xrange(l):
				if img[i][j] == 0:
					img[i][j] = lt.get_ignore_label()
				else:
					img[i][j] = lt.part2unicode(label, img[i][j])
		#analyse_image(img)

		datum = caffe.proto.caffe_pb2.Datum()
		datum.channels = 1
		datum.height = l
		datum.width = l 

		#img = img.transpose(2, 0, 1)
			
		datum.float_data.extend(img.reshape(img.size).tolist()) # shrunk the array to a list and save it
		#datum.data = img.tobytes()

		str_id = '{:08}'.format(cnt)
		txn.put(str_id.encode('ascii'), datum.SerializeToString())

		cnt = cnt + 1
		#if cnt == 20:
			#break
print cnt
