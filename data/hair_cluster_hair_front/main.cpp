//
//  main.cpp
//
//  Created by Liwen on 11/12/15.
//  Copyright (c) 2015 Liwen. All rights reserved.
//

#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <cstdlib>
#include <direct.h>
#include <ctime>
#include <io.h>
#include <fstream>
#include <sys/stat.h>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

using namespace cv;
using namespace std;

void generate(const Mat &hair, const Mat &hairMask,
              const int offsetX, const int offsetY,
              const float offsetH, const float offsetS, const float offsetV,
              Mat &dest, Mat &destMask, Mat &destHair)
{
    Mat hair_hsv, hair_rgb;
    hair_rgb = hair;
    cvtColor(hair_rgb, hair_hsv, CV_RGB2HSV);
	//cout << "begin" << endl;
    for (int j = 0; j < hair_hsv.rows; j++)
    {
        for (int i = 0; i < hair_hsv.cols; i++)
        {
            Vec3b& b = hair_hsv.at<Vec3b>(Point(i, j));
            const Vec3b alpha = hairMask.at<Vec3b>(Point(i, j));
            if (alpha[0] == 0) continue;
            if (offsetH > 0.0) b[0] = (uchar)(((int)b[0] + (int)((255.0f - b[0]) * offsetH)) % 256);
            else b[0] = (uchar)(((int)b[0] + (int)(b[0] * offsetH)) % 256);
            if (offsetS > 0.0) b[1] = (uchar)(((int)b[1] + (int)((255.0f - b[1]) * offsetS)) % 256);
            else b[1] = (uchar)(((int)b[1] + (int)(b[1] * offsetS)) % 256);
            if (offsetV > 0.0) b[2] = (uchar)(((int)b[2] + (int)((255.0f - b[2]) * offsetV)) % 256);
            else b[2] = (uchar)(((int)b[2] + (int)(b[2] * offsetV)) % 256);
        }
    }
	//cout << "cvtcolor" << endl;
	cvtColor(hair_hsv, hair_rgb, CV_HSV2RGB);


    for (int j = 0; j < hair_rgb.rows; j++)
    {
        for (int i = 0; i < hair_rgb.cols; i++)
        {
            if (i + offsetX < 0 || i + offsetX >= dest.cols || j + offsetY < 0 || j + offsetY >= dest.rows) continue;
            const Vec3b b = hair_rgb.at<Vec3b>(Point(i, j));
            if (b[0] == 0 && b[1] == 0 && b[2] == 0) continue;
            dest.at<Vec3b>(Point(i + offsetX, j + offsetY)) = b;
            //const Vec3b alpha = hairMask.at<Vec3b>(Point(i, j));
            //destMask.at<Vec3b>(Point(i + offsetX, j + offsetY)) = alpha;
        }
    }
    Mat trans_mat = (Mat_<double>(2,3) << 1, 0, offsetX, 0, 1, offsetY);

	//cout << "wrap" << endl;
    warpAffine(hairMask, destMask, trans_mat, hairMask.size());


	for (int j = 0; j < dest.rows; j++)
	{
		for (int i = 0; i < dest.cols; i++)
		{
			Vec3b &a = destHair.at<Vec3b>(Point(i, j));
			const Vec3b &b = dest.at<Vec3b>(Point(i, j));
			const Vec3b alpha = destMask.at<Vec3b>(Point(i, j));
			if (alpha[0] == 0) continue;
			a = b;
		}
	}
}

bool is_front_file (string file_name) {
    string sNumber = file_name.substr(file_name.size() - 8, 4);
    int number = atoi(sNumber.c_str());

    if ((number < 72 ) || (number > 152)) return false; /* The region of center.*/

    int position = number % 9;

    if ((position == 0) || (position == 1) || (position == 7) || (position == 8)) return false;

    return true;
}

void usage(const char* myname)
{
    printf("Usage: \t%s hair_list.txt background_list.txt [iteration num] \n", myname);
    exit(1);
}


int main( int argc, char** argv )
{
    cout << "FUCK" << endl;
    if (argc < 3)
    {
        usage(argv[0]);
    }

    srand(time(NULL));

    vector<string> hairFolder;
    ifstream ifs;
    string line;
    ifs.open(argv[1]);
    while (std::getline(ifs, line)) hairFolder.push_back(line);
    ifs.close();

    vector<string> hairFile;
    for (int i = 0; i < hairFolder.size(); i++)
    {
        for (int j = 0; j < 225; j++)
        {
            char fileName[200];
            sprintf_s(fileName, "../hair_cluster/%s/%s_%04d.png", hairFolder[i].c_str(), hairFolder[i].c_str(), j);
            hairFile.push_back(fileName);
        }
    }

	cout << "Load all hair folders" << endl;



	cout << "Start to transfer data" << endl;
    for (int it = 1; it < 2; it++)
    {

        char folderName[200];
        sprintf_s(folderName, "cnn%02d", it);
		cout << "Iteration " << it << ", which process strand " << folderName << endl;
        _mkdir(folderName);
        string pre = folderName;
        for (int i = 0; i < hairFolder.size(); i++)
        {
            _mkdir((pre + "/" + hairFolder[i]).c_str());
        }
        const int num_duplicate = 4;
        for (int i = 0; i < hairFile.size(); i++)
        {
            Mat hair, hairMask,dest, destMask;
			if (_access(hairFile[i].c_str(), 0) == -1) continue;
            if (!(is_front_file(hairFile[i]))) continue;
            for (int iter_duplicate = 0; iter_duplicate < num_duplicate; iter_duplicate++) {
                hair = imread(hairFile[i]);
                //cout << "read hair" << endl;
                hairMask = imread(hairFile[i].substr(0, hairFile[i].size() - 4) + "_mask.png");
                //int bgIdx = rand() % backgroundFile.size();
                //background = imread("./background/" + backgroundFile[bgIdx]);
                dest = Mat::zeros(hair.size(), CV_8UC3);

                destMask = hairMask;
                Mat destHair = Mat::zeros(hair.size(), CV_8UC3);

                const int range = 10;
                const int mid = 5;
                int offsetX = rand() % range - mid;
                int offsetY = rand() % range - mid;
                float offsetH = rand() / (float)RAND_MAX * 0.4f - 0.2f; //set hue offset range to be [-0.2f, 0.2f] to avoid rare hair color.
                float offsetS = rand() / (float)RAND_MAX * 1.0f - 0.5f;
                float offsetV =  rand() / (float)RAND_MAX * 1.0f - 0.5f;

                generate(hair, hairMask, offsetX, offsetY, offsetH, offsetS, offsetV, dest, destMask, destHair);

                //cout << "end" << endl;
                //cout << pre << endl;
                const int magic_number = 16; /*It's the number of prefix */
                char temp[5];
                sprintf_s(temp, "%02d", iter_duplicate);
                string new_file_name = pre + "/" + hairFile[i].substr(magic_number, hairFile[i].size() - 4) + "_" + temp + ".png";
                string new_mask_name = new_file_name.substr(0, new_file_name.size() - 4) + "_mask.png";
                string new_hair_name = new_file_name.substr(0, new_file_name.size() - 4) + "_hair.png";
                //cout << pre + "/" + hairFile[i].substr(magic_number, hairFile[i].size()) << endl;
                //cout << pre + "/" + hairFile[i].substr(magic_number, hairFile[i].size() - 4) + "_mask.png" << endl;
                imwrite(new_file_name, dest);
				imwrite(new_mask_name + "_mask.png", destMask);
				imwrite(new_hair_name + "_hair.png", destHair);
            }
            cout<<hairFile[i]<<" finished"<<endl;
        }
    }

    return 0;
}
