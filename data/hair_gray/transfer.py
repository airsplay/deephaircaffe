# Transfer the origin input to a gray-scale input
# Only run once
import os
import shutil
def transfer(f):
	shutil.copyfile(f, f+"Origin")
	origin = open(f+"Origin", 'r')
	out = open(f, 'w')
	t = origin.readlines()
	for line in t:
		path, tag = line.split()
		d, f1 = os.path.split(path)
		name, ext = os.path.splitext(f1)
		out.write(os.path.join(d, name + "_gray" + ext) + " " + tag + "\n")
if __name__ == "__main__":
	transfer("trainInput")
	transfer("testInput")
