import numpy as np
import cv2 as cv
import caffe
import random
import os

def process(phase):
	name = phase + "Input"
	f = open(name, 'r')
	f1 = open(os.path.join("hair_random", name), 'w')
	f2 = open(os.path.join("hair_random_mask", name), 'w')
	lines = f.readlines()
	random.shuffle(lines)
	for line in lines:
		path, label = line.split()
		folder, f = os.path.split(path)
		fileName, ext = os.path.splitext(f)
		f1.write(line)
		f2.write(os.path.join(folder, fileName + "_mask01" + ext) + " " + label + "\n")
		
		#print line

process("train")
#process("test")
