from hairIterator import *
from cluster_mask import *
from analysis import *
import os
import numpy as np
import pickle
class LabelTransfer:
	p2u_file_name = "part2unicode.pkl"
	u2p_file_name = "unicode2part.pkl" 
	def __init__(self):
		self.p = os.path.split(os.path.realpath(__file__))[0]
		a =	os.path.join(self.p, self.p2u_file_name)  # The actual path of p2u_file_name
		if os.path.exists(a):
			self._part2unicode = pickle.load(open(a, "rb"))
		else:
			print "There is no " + self.p2u_file_name
		b =	os.path.join(self.p, self.u2p_file_name)  # The actual path of u2p_file_name
		if os.path.exists(b):
			self._unicode2part = pickle.load(open(b, "rb"))
		else:
			print "There is no " + self.u2p_file_name
		self._unicode_total = len(self._unicode2part)
		#print self._part2unicode
		#print self._unicode2part

	def create(self):
		self._part2unicode_file = open(self.p2u_file_name, 'wb')
		self._unicode2part_file = open(self.u2p_file_name, 'wb')
		path_ex = ""
		label_set = set()
		parser = HairParser()
		self._part2unicode = {}
		self._unicode2part = {}
		self._unicode_total = 0
		path = ""
		for path, file_name in HairIterator():
			if path == path_ex:
				pass
			else:
				self._part2unicode[path_ex] = {}
				for label in sorted(list(label_set)):
					if label == 0:
						continue
					else:
						self._unicode2part[self._unicode_total] = (path_ex, label)
						self._part2unicode[path_ex][label] = self._unicode_total
						self._unicode_total += 1
				#if "3"in path:
					#break
				#print self._unicode2part
				#print self._part2unicode
				path_ex = path
				label_set = set()
				print path
			label = open_cluster_mask(os.path.join(path, parser.file2mask(file_name)))
			label = label / 32
			labels = np.unique(label).tolist()
			label_set = label_set.union(labels)
			#print label_set
			#label_set += labels
			
		self._part2unicode[path] = {}
		for label in sorted(list(label_set)):
			if label == 0:
				continue
			else:
				self._unicode2part[self._unicode_total] = (path, label)
				self._part2unicode[path][label] = self._unicode_total
				self._unicode_total += 1
		pickle.dump(self._unicode2part, self._unicode2part_file)
		pickle.dump(self._part2unicode, self._part2unicode_file)
			
			
			

			
	def part2unicode(self, path, label):
		if isinstance(path, int):
			path = "D:\\airsplay\\HairTrain\\data\\hair_cluster\\strands%05d" % path
		#print path, label
		return self._part2unicode[path][label]
	def image_u2p(self, img):
		#img = np.zeros((datum.height, datum.width))
		for i in xrange(img.shape[0]):
			for j in xrange(img.shape[1]):
				if img[i][j] == self.get_ignore_label():
					img[i][j] = 0
				else:
					path, img[i][j] = self.unicode2part(img[i][j])
		img *= 32
		return img
	def unicode2part(self, u):
		return self._unicode2part[u]
	def part322unicode(self, part, label):
		#return self._part2unicode[part][label/32]
		return self.part2unicode(part, label/32)
	def unicode2part32(self, u):
		return (lambda (x,y):(x, y*32)) (self.unicode2part(u))
	def unicode_total(self):
		return self._unicode_total
	# Use the maximum_unicode + 1 as the ignore label
	def get_ignore_label(self):
		return self.unicode_total()
	def test(self):
		for i in xrange(self.unicode_total()):
			print i
			style, num = self.unicode2part(i)
			print style, num
			u = self.part2unicode(style, num)
			print u
			print ""
	def test1(self):
		for i in xrange(self.unicode_total()):
			#print self.unicode2part(3)
			style, num = self.unicode2part(i)
			#u = self.part2unicode(2, 1)
			#u = self.part322unicode(2, 32)
			if self.part2unicode(style, num) != self.part322unicode(style, num * 32):
				print "FUCK"
if __name__ == "__main__":
	l = LabelTransfer()
	#l.test()
	print len(l._unicode2part)
	print l.unicode_total()
	print l.get_ignore_label()


