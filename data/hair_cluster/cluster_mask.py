import cv2 as cv
import os
import numpy as np
from hairTools import *
from hairIterator import *
from analysis import *


def open_cluster_mask(img):
	if isinstance(img, str):	# The parameter is a file_name
		img = cv.imread(img)
	img_0 = img[:,:,0]
	#analyse_image(img_0)
	#show_image(img_0)
	return img_0
	#s = img.tostring()
	#print s
	#np.savetxt("haha.txt", img, fmt = "%s", delimiter = ",")
	#s = np.array2string(img)
	#print s

def test_cluster_mask():
	parser = HairParser()
	for path, file_name in HairIterator():
		print path, file_name
		img = open_cluster_mask(os.path.join(path, parser.file2mask(file_name)))


if __name__ == "__main__":
	#open_cluster_mask(os.path.join("strands00001", "strands00001_0000_mask.png"))
	test_cluster_mask()
