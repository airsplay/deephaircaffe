import os
import urllib2
import urllib
import re
import zipfile

html = urllib2.urlopen ("https://www.dropbox.com/sh/ss2vfkb2pyimbuw/AAASOG2YilH8oPCTaJKNNClQa?dl=0").read()

result = re.findall(r"<a href=\"(https://www\.dropbox\.com/sh/ss2vfkb2pyimbuw/[^\"]*)", html)

#print result

result = list(set(result))		# Unique
print result
result.sort()


result = filter(lambda x:"strands" in x, result)
fileName = [re.search("strands\d*", url).group(0) for url in result]
#print fileName

comb = zip(fileName, result)
comb.sort()

for name, url in comb:
	if name != "strands00020":
		continue
	if os.path.exists(name + ".zip") :
		print name + ".zip has been downloaded"
		continue

	else :
		print "Start to Retrieve " + name + ".zip"
		urllib.urlretrieve(url[:-1] + "1", name + ".zip")
	
	print "unzip " + name + ".zip"
	if not(os.path.exists(name)):
		os.mkdir(name)
	zFile = zipfile.ZipFile(name + ".zip" , 'r')
	for fileName in zFile.namelist()[1:]:
		data = zFile.read(fileName)
		f = open(name + "/" + fileName, 'w+b')
		f.write(data)
		f.close()
	
for name, url in comb:
	os.remove(name + ".zip")

#print re.search("h", url).groups()
#<a href="https://www.dropbox.com/sh/bddi53a2fb6qadk/AABNsmIzbZ3xjTdGnGt4EN1Ea/strands00041?dl=0" target="_top" rel="nofollow" class="file-link filename-link"><span id="pyxl932">strands00041</span></a>

#print url
#urllib.urlretrieve("https://www.dropbox.com/sh/bddi53a2fb6qadk/AAAzdNaHMhmw_LLYHGPGbSSqa/strands00002?dl=1", "hehe.zip")
