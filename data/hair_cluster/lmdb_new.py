import numpy as np
import lmdb
import caffe
import cv2 as cv
import hair_cluster
import os
import random

#Get the files for hair_cluster

all_files = [os.path.join(path, file_name) for path, file_name in hair_cluster.HairIterator()]
random.shuffle(all_files)

map_size = len(all_files) * 227*227*3 * 4

print "The map_size", map_size
print "The total files are", len(all_files)

#Create lmdb
env = lmdb.open("train_lmdb", map_size = map_size)

cnt = 0 
with env.begin(write=True) as txn:
	for path in all_files:
		if cnt % 1000 == 0:
			print cnt

		img = cv.imread(path)
		img = cv.resize(img, (args.size, args.size))

		datum = caffe.proto.caffe_pb2.Datum()
		datum.channels = img.shape[2]
		datum.height = args.size
		datum.width = args.size

		img = img.transpose(2, 0, 1)
			
		#datum.float_data.extend(img.reshape(img.size).tolist())
		datum.data = img.tobytes()

		str_id = '{:08}'.format(cnt)
		txn.put(str_id.encode('ascii'), datum.SerializeToString())

		cnt = cnt + 1
print cnt
