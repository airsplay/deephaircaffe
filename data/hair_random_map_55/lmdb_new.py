import numpy as np
import lmdb
import caffe
import cv2 as cv
import argparse

arg_parser = argparse.ArgumentParser()
arg_parser.add_argument("inputFile", help = "The input to parser")
arg_parser.add_argument("outFile", help = "The db name of outfile")
arg_parser.add_argument("--gray", help = "The gray scale image", action = "store_true")
arg_parser.add_argument("--float", help = "Store in float_data", action = "store_true")
args = arg_parser.parse_args()

all_files = open(args.inputFile).readlines()
# Get the information of images
path, label = all_files[0].split()
if args.gray:
	img = cv.imread(path, -1)
else:
	img = cv.imread(path)
# The size. 8 is the maximum(the size of double)
map_size = len(all_files) * img.size * 8
env = lmdb.open(args.outFile, map_size = map_size)

print "The total files are", len(all_files)
cnt = 0 
with env.begin(write=True) as txn:
	for line in all_files:
		if cnt % 1000 == 0:
			print cnt
		if cnt < 60000:
			cnt += 1
			continue
		path, label = line.split()
		label = int(label)
		datum = caffe.proto.caffe_pb2.Datum()
		if args.gray:
			img = cv.imread(path, -1)
			datum.channels = 1
		else:
			img = cv.imread(path)
			datum.channels = img.shape[2]
		datum.height = img.shape[0]
		datum.width = img.shape[1]
		if args.float:
			datum.float_data.extend(img.reshape(img.size).tolist())
		else:
			datum.data = img.tobytes()
		str_id = '{:08}'.format(cnt)
		txn.put(str_id.encode('ascii'), datum.SerializeToString())
		cnt = cnt + 1
print cnd
