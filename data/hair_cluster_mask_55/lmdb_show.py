import numpy as np
import lmdb
import caffe
from hairTools import *
import hair_cluster

env = lmdb.open('train_lmdb', readonly = True)
lt = hair_cluster.LabelTransfer()
with env.begin() as txn:
	cursor = txn.cursor()
	for k, v in cursor:
		print k 
		datum = caffe.proto.caffe_pb2.Datum()
		datum.ParseFromString(v)
		print datum.channels, datum.height, datum.width#, datum.float_data
		flat_x = np.array(datum.float_data) #np.fromstring(datum.data, dtype=np.uint8)
		x = flat_x.reshape(datum.height, datum.width)
		"""
		img = np.zeros((datum.height, datum.width))
		for i in xrange(datum.height):
			for j in xrange(datum.width):
				if x[i][j] == lt.get_ignore_label():
					img[i][j] = 0
				else:
					path, img[i][j] = lt.unicode2part(x[i][j])
		img *= 32
		"""
		img = lt.image_u2p(x)
		y = datum.label
		#show_image(x.transpose(1,2,0))
		analyse_image(x)
		analyse_image(img)
		show_image_plt(img)
