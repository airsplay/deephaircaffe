import os
import sys
import re
import analysis



dataPath = "D:\\airsplay\\HairTrain\\data\\background\\image"
dataDirs = os.listdir(dataPath)

class ImageIterator:
	def __init__(self):
		self.parser = analysis.HairParser()	

	def __iter__(self):
	# Convert this class to an Iterator
		return self.generator()

	def generator(self):
	# The generator to the files
		files = os.listdir(dataPath)
		
		for f in files:
			yield [dataPath, f]

# Here is an example
if __name__ == "__main__":
	for path, f in ImageIterator():
		print path, f
