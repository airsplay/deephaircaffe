import numpy as np
import lmdb
import caffe
import cv2 as cv
import os
import random
from analysis import *
import image_iterator
from tqdm import tqdm

#Get the files for hair_cluster
all_files = [os.path.join(path, name) for path, name in image_iterator.ImageIterator()]



loop_num = 20
map_size = loop_num * len(all_files) * 512*512* 4 * 2

print "The map_size", map_size
print "The total files are", len(all_files)

env = lmdb.open("train_lmdb", map_size = map_size)

cnt = 0 
with env.begin(write=True) as txn:
	for loop_iter in tqdm(range(loop_num)):
		random.shuffle(all_files)
		for file_name in all_files:
			l = random.randint(227, 512) # random get size from 227 to 256

			background = cv.resize(cv.imread(file_name), (l, l))
			mask = np.zeros((background.shape[0], background.shape[1]), dtype = np.uint8)
			mask = mask[np.newaxis, :]

			background = background.transpose(2, 0, 1)
			img = np.concatenate((background, mask), axis = 0)

			#print img.shape
			




			datum = caffe.proto.caffe_pb2.Datum()
			datum.channels = img.shape[0]
			datum.height = l
			datum.width = l 
			
			#print datum.channels, datum.height, datum.width

			#img = img.transpose(2, 0, 1)
				
			#datum.float_data.extend(img.reshape(img.size).tolist())
			datum.data = img.tobytes()

			str_id = '{:08}'.format(cnt)
			txn.put(str_id.encode('ascii'), datum.SerializeToString())
			cnt += 1

