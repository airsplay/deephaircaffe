from hairTools import *
import os
import numpy as np
import cv2 as cv
import random
import shutil

def generate_mask():
	label_dir = "parts_lfw_funneled_gt_images"
	out_dir = "mask"
	file_list = os.listdir(label_dir)
	for f in file_list:
		print f
		if f[0] == ".":
			continue
		img = read_ppm(os.path.join(label_dir, f))
		img = img / 25


		hair = img[:,:,0]
		file_name, file_ext = os.path.splitext(f)
		cv.imwrite(os.path.join(out_dir, file_name + ".png"), hair)
		
def generate_train():
	mask_dir = "mask"
	image_dir = "lfw_funneled"
	data_dir = "data"
	file_list = os.listdir(mask_dir)
	random.shuffle(file_list)	#Do shuffle on the file list
	f_data = open("dataTrain.txt", 'w')
	f_mask = open("maskTrain.txt", 'w')
	now = os.getcwd()
	for f in file_list:
		file_name, file_ext = os.path.splitext(f)
		f_mask.write(os.path.join(now, mask_dir, f) + " 0\n")
		data_path = os.path.join(now, image_dir, file_name[:-5], file_name + ".jpg")
		if not(os.path.exists(data_path)):
			print "The path " + data_path + " doesn't exist"
		shutil.copy(data_path, os.path.join(data_dir, file_name + ".jpg"))
		f_data.write(data_path + " 0\n")

def generate_selected():
	mask_dir = "mask"
	data_dir = "selected"
	s_data = open("selectedData", 'w')
	s_mask = open("selectedMask", 'w')
	s_hair = open("selectedHair", 'w')
	now = os.getcwd()
	for f in os.listdir(data_dir):
		if "_hair.png" in f:
			continue
		file_name, file_ext = os.path.splitext(f)
		data_path = os.path.join(now, data_dir, f)
		s_data.write(data_path + " 0\n")
		mask_path = os.path.join(now, mask_dir, file_name + ".png")	

		print mask_path
		data = cv.imread(data_path)
		mask = cv.imread(mask_path)

		hair = (mask * 255) & data
		if not(os.path.exists(mask_path)):
			print "No such mask: " + mask_path
		s_mask.write(mask_path + " 0\n")

		hair_path = os.path.join(now, data_dir, file_name + "_hair.png")
		save_image(hair_path, hair)
		s_hair.write(hair_path + " 0\n")
		
if __name__ == "__main__":
	generate_selected()

		
