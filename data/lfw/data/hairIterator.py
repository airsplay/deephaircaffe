import os
import sys
import re
import analysis

dataPath = os.path.split(os.path.realpath(__file__))[0]#"D:\\airsplay\\HairTrain\\data\\hair_random\\cnn00"
dataDirs = os.listdir(dataPath)

class HairIterator:
	def __init__(self, dict_form = False):
		self.parser = analysis.HairParser()	
		self.dict_form = dict_form

	def __iter__(self):
	# Convert this class to an Iterator
		return self.generator()

	def generator(self):
	# The generator to the files
		files = os.listdir(dataPath)
		#print files
		
		for f in files:
			if (".jpg" in f) and not("hair" in f):
				if self.dict_form:
					d = dict()
					d['path'] = dataPath #os.path.join(dataPath, name)
					d['file'] = f
					d['mask'] = f[:-4] + ".png"
					#d['hair'] = f[:-4] + "_hair.png"#self.parser.file2hair(f)
					yield(d)
					#yield (os.path.join(dataPath, name), f, self.parser.file2mask(name))
				else:
					yield (os.path.join(dataPath, name), f)

# Here is an example
if __name__ == "__main__":
	for d in HairIterator(dict_form = True):
		print d
