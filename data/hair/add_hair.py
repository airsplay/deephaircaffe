from hairIterator import *
import cv2 as cv
import os
from hairTools import *

last = ""
for path, f in HairIterator():
	if (path != last):
		last = path
		get_hair(path)
