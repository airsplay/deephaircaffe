from hairIterator import *
import cv2 as cv
import os

for path, f in HairIterator():
	img = cv.imread(os.path.join(path, f))
	img_gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
	name, ext = os.path.splitext(f)
	gray_file_name = name + "_gray" + ext
	cv.imwrite(os.path.join(path, gray_file_name), img_gray)
	cv.waitKey()
