#Hair Data
##Tools
You can follow the following order of command to generate the data.
###download.py
It downloads the data from Dropbox.
It crashes somtime... You can restart it. The program will continue to download.
```bash
	python download.py
```
###Generator.py
It generates testInput and trainInput for trainning.
The default ratio of trainning set:test set is 7:3. You can easily change it in the head of this code.
```bash
	python generator.py trainInput testInput
	// or just click on generator.bat if you use windows
```
###Convert_to_leveldb and Convert_to_lmdb
It generates lmdb or leveldb using caffe/tools/convert_imageset.
It has been compiled to caffe/bin in my computer.


##Files
1. testInput/testOutput
	The un-normalized image-label pairs
2. testInput_origin/trainInput_origin 
	The normalized image-label pairs
	
##Folders
1. strands00XXX
	The data for the strand labeld XXX.
	The label is not its origin after normalization.
2. train_lmdb / test_lmdb
	The data for lmdb
3. train_leveldb / test_leveldb
	The data for leveldb