# Generate the hair_constant which choose the test as a continous sequense
import os
import sys
import re
import random

ratioOfTrain = 0.6						# The ratio of data used in training
trainFilePath = "trainInput2"
testFilePath = "testInput2"

def usage():
	print "python generator.py trainFilePath testFilePath"
	print "PLEASE use the full path of trainFile and testFile"

def listGet(c, a, b):
	a = a % len(c)
	b = b % len(c)
	if a > b:
		return c[a:] + c[:b]
	else:
		return c[a:b]

dataPath = os.path.dirname(sys.argv[0]) # This python script is in the same folder of data file

if dataPath == "":
	dataPath = os.path.abspath('.')

if len(sys.argv) > 1:
	trainFilePath = sys.argv[1]
	testFilePath = sys.argv[2]



trainFile = open(trainFilePath, "w")
testFile = open(testFilePath, "w")

# List the dir of data
#os.chdir("./data")
dataDirs = os.listdir(dataPath)
#print dataDirs

totalNumber = 0 
# For each data, generate train and test config
cnt = 0
for name in filter(lambda x:os.path.isdir(os.path.join(dataPath, x)), dataDirs): # Filter the folders
	#print name
	matchPatch = re.match(r"strands(\d+)", name)
	if matchPatch == None:
		continue
	tag = int(matchPatch.group(1))					#Use XXXX of stransXXXX as tag
	totalNumber = max(tag, totalNumber)

	files = os.listdir(os.path.join(dataPath, name))
	maskList = filter(lambda x:"mask" in x, files)
	dataList = filter(lambda x:not ("mask" in x), files)
	#random.shuffle(dataList)
	
	path = os.path.join(dataPath, name)
	#print len(dataList)
	if len(dataList) == 0:
		print "No data in "+ name
		continue
	partition = random.randint(0, len(dataList) - 1)
	trainNum = int(ratioOfTrain * len(dataList))

	configOfTrain = reduce(lambda x, y: x + os.path.join(path, y)  + " " + str(tag) + "\n", listGet(dataList, partition, partition + trainNum), "")
	configOfTest = reduce(lambda x, y: x + os.path.join(path, y) + " " + str(tag) + "\n", listGet(dataList, partition + trainNum , partition), "")

	#print configOfTrain
	#os.system("pause")
	
	
	trainFile.write(configOfTrain)
	testFile.write(configOfTest)
	cnt = cnt + 1

print cnt


trainFile.close()
testFile.close()
