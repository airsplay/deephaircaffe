from hairIterator import *
from analysis import *
import cv2 as cv
import os
import numpy as np

last = ""
parser = HairParser()
for path, f in HairIterator():
	if last != path:
		print path
		last = path
	mask = parser.file2mask(f)
	img = cv.imread(os.path.join(path,mask), 0)
	#print img.shape
	#print img.max()
	#print img.sum()
	s = np.unique(img)
	#print s
	one =  np.ones(img.shape, dtype = np.uint8)
	img = img / 255 #& one
	#print img.sum()
	mask01 = parser.file2any(f, "mask01")
	#print mask01
	#cv.namedWindow("show")
	#cv.imshow("show", img * 255)
	#cv.waitKey()
	cv.imwrite(os.path.join(path, mask01), img)
	#break
