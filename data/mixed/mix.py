import random
mixedRate = 7/1

sed = open("selectedData")
sem = open("selectedMask")

syd = open("syntheticData")
sym = open("syntheticMask")


se = zip(sed.readlines(), sem.readlines())
sy = zip(syd.readlines(), sym.readlines())

random.shuffle(sy)

total = sy[:len(se) * mixedRate] + se

random.shuffle(total)
print len(se), len(total)

td, tm = zip(*total)

open("mixedData", 'w').writelines(td)
open("mixedMask", 'w').writelines(tm)
