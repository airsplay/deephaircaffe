from analysis import *
from hairTools import *
import shutil
import os
p = HairParser()
lines = open("trainInput").readlines()
shutil.copy("trainInput", "trainInputBeforeHair")
f = open("trainInput", 'w')
for line in lines:
	path, label = line.split()
	dire, name = os.path.split(path)
	name = p.file2hair(name)
	f.write(os.path.join(dire, name) + " " + str(label) + "\n")
	
	
