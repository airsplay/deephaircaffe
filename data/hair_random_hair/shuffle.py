import random
import shutil

def shuffle_and_output(name):
	shutil.copy(name, name + "_before_shuffle")
	file_list = open(name).readlines()
	random.shuffle(file_list)
	open(name, 'w').writelines(file_list)

map(shuffle_and_output, ["hairInput"])
