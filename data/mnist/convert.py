import numpy as np
import cv2 as cv
import os

class Read_Stream:
	def __init__(self, data):
		self.now = 0
		self.data = data

	def read_int(self):
		i = (self.data[self.now] << 24) + (self.data[self.now + 1] << 16) + (self.data[self.now + 2] << 8) + self.data[self.now + 3]
		self.now = self.now + 4
		return i

	def read_byte(self):
		b = self.data[self.now]
		self.now = self.now + 1
		return b
		

class Convert_Minst:
	def __init__(self):
		pass

	def convert_image(self, fileName, outName):
		s = open(fileName, "rb").read()
		data = map(lambda x:ord(x), s)
		stream = Read_Stream(data)
		#print data
		if not(os.path.exists(outName)):
			os.mkdir(outName)
		magic, size, rows, columns = [stream.read_int() for i in xrange(4)]
		print magic, size, rows, columns

		if not(magic == 2051):
			print "Wrong images file"
			return

		for cnt in xrange(size):
			imgData = np.array([stream.read_byte() for i in xrange(rows * columns)], np.uint8)
			imgData = imgData.reshape(rows, columns)
			#print imgData
			#self.show_image(imgData)
			cv.imwrite(os.path.join(outName, str(cnt) + ".png"), imgData)
	
	def show_image(self, data):
		cv.namedWindow("show")
		cv.imshow("show", data)
		cv.waitKey()
		cv.destroyWindow("show")
		
		
	

if __name__ == "__main__":
	convert = Convert_Minst()
	convert.convert_image("./origin/train-images-idx3-ubyte", "train")
	convert.convert_image("./origin/t10k-images-idx3-ubyte", "test")
