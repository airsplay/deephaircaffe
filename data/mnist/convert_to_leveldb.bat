
convert_imageset "" trainInput --backend="leveldb" --resize_height=227 --resize_width=227 --shuffle=true train_leveldb

convert_imageset "" testInput --backend="leveldb" --resize_height=227 --resize_width=227 --shuffle=true test_leveldb

pause