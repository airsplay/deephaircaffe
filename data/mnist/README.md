[TOC]
#Minst


##Download
If you use windows, please use gitbash to run it.
```bash
	get_mnist.sh
```
##Convert
convert origin data to image files
```bash
	python convert.py
```
##Generator 
Generate trainInput and testInput
```bash
	python generator.py
```
##Conver to lmdb data base 
It calls the conver_imageset.exe in caffe/bin to change the dataset to lmdb.
```bash
	conver_to_lmdb.bat
```


#Files
	
	testInput/trainInput : the file / label pairs
#Folders
	test/train folder : the images of  mnist
	mnist-test-leveldb/mnist-train-leveldb : the leveldb version of mnist downloaded from website
	test_lmdb/train_lmdb : the lmdb version of data
	orgin : origin data downloaded from website