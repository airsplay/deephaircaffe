import numpy as np
import cv2 as cv
import os

class Read_Stream:
	def __init__(self, data):
		self.now = 0
		self.data = data

	def read_int(self):
		i = (self.data[self.now] << 24) + (self.data[self.now + 1] << 16) + (self.data[self.now + 2] << 8) + self.data[self.now + 3]
		self.now = self.now + 4
		return i

	def read_byte(self):
		b = self.data[self.now]
		self.now = self.now + 1
		return b
		

class Generator:
	def __init__(self):
		pass

	def generate(self, fileName, inDir, outName):
		s = open(fileName, "rb").read()
		data = map(lambda x:ord(x), s)
		stream = Read_Stream(data)
		magic, size = [stream.read_int() for i in xrange(2)]

		if not(magic == 2049):
			print "Wrong images file"
			return

		f = open(outName, "w")
		if not(os.path.exists(inDir)):
			print "The path " + inDir + " doesn't exist"

		inDir = os.path.abspath(inDir)
		for cnt in xrange(size):
			label = stream.read_byte()
			f.write(os.path.join(inDir, str(cnt) + ".png") + " " + str(label) + "\n")
			

if __name__ == "__main__":
	gen = Generator()
	gen.generate("./origin/train-labels-idx1-ubyte", "train", "trainInput")
	gen.generate("./origin/t10k-labels-idx1-ubyte", "test", "testInput")
