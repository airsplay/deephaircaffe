from hairIterator import *
from hairTools import *
from analysis import *
import cv2 as cv
import os


p = HairParser()
out = open("hairInput", "w")
for path, f in HairIterator():
	img = cv.imread(os.path.join(path, f))
	mask = cv.imread(os.path.join(path, f + "_mask.png"))
	hair = img & mask
	#show_image(hair)
	save_image(os.path.join(path, p.file2hair(f)), hair)
	label, cnt = p.parse(f)
	out.write(os.path.join(path, p.file2hair(f)) + " " + str(label) + "\n")
	
	
