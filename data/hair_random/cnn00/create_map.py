from hairIterator import *
from hairTools import *
from analysis import *
import cv2 as cv
import os

COLOR_BACKGROUND = 341 # The magic number indicate the background

p = HairParser()
t = HairLabelTransformer()

for path, f in HairIterator():
	mask01_name = p.file2mask01(f)
	map_name = p.file2any(f, "map")
	mask01 = cv.imread(os.path.join(path, mask01_name), -1) #-1 means gray-scale image
	
	background01 = ~mask01 / 255
	print background01, background01.sum()
	print mask01, mask01.sum()
	
	mask01_55 = cv.resize(mask01, (55, 55))
	analyse_image(mask01_55, "mask01_55")
	background01_55 = ~mask01_55 / 255#cv.resize(background01, (55, 55))

	label, cnt = p.parse(f)
	label = t.real2caffe(label)

	map_55 = background01_55 * COLOR_BACKGROUND + mask01_55 * label

	analyse_image(map_55)

	save_image(os.path.join(path, p.file2any(f, "map")), map_55)

	
	#show_image(hair)
	
	
