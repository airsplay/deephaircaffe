from hairIterator import *
from analysis import *
import cv2 as cv
import os
import numpy as np

last = ""
parser = HairParser()
for path, f in HairIterator():
	if last != path:
		print path
		last = path
	label, cnt = parser.parse(f)
	if label < 296:
		continue
	#print path, f
	if parser.match(f):
		pass
	else:
		continue
	mask = f + "_mask.png"#parser.file2mask(f)
	#print mask
	img = cv.imread(os.path.join(path,mask), 0)
	#print img.shape
	#print img.max()
	#print img.sum()
	s = np.unique(img)
	#print s
	one =  np.ones(img.shape, dtype = np.uint8)
	img = img / 255 #& one
	#print img.max()
	#print np.average(img)
	#print img.sum()
	mask01 = parser.file2any(f, "mask01")
	#print mask01
	#cv.namedWindow("show")
	#cv.imshow("show", img * 255)
	#cv.waitKey()
	cv.imwrite(os.path.join(path, mask01), img)
	#break
