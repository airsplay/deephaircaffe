from hairIterator import *
from hairTools import *
from analysis import *

p = HairParser()
t = HairLabelTransformer()
out = open("trainInput", 'w')
for path, f in HairIterator():
	label, cnt = p.parse(f)
	label = t.real2caffe(label)
	out.write(os.path.join(path, f) + " " + str(label) + "\n")
	
	
	
