import os
import sys
import re
import random

ratioOfTrain = 1						# The ratio of data used in training
trainFilePath = "trainInput"
testFilePath = "testInput"

def usage():
	print "python generator.py trainFilePath testFilePath"
	print "PLEASE use the full path of trainFile and testFile"

dataPath = os.path.dirname(sys.argv[0]) # This python script is in the same folder of data file

if dataPath == "":
	dataPath = os.path.abspath('.')

if len(sys.argv) > 1:
	trainFilePath = sys.argv[1]
	testFilePath = sys.argv[2]


trainFile = open(trainFilePath, "w")
testFile = open(testFilePath, "w")

# List the dir of data
#os.chdir("./data")
dataDirs = os.listdir(dataPath)
#print dataDirs

totalNumber = 0 
# For each data, generate train and test config
for name in filter(lambda x:os.path.isdir(os.path.join(dataPath, x)), dataDirs): # Filter the folders
	matchPatch = re.match(r"strands(\d+)", name)
	tag = int(matchPatch.group(1))					#Use XXXX of stransXXXX as tag
	totalNumber = max(tag, totalNumber)

	files = os.listdir(os.path.join(dataPath, name))
	maskList = filter(lambda x:"mask" in x, files)
	dataList = filter(lambda x:not ("mask" in x), files)
	random.shuffle(dataList)
	
	path = os.path.join(dataPath, name)
	configOfTrain = reduce(lambda x, y: x + os.path.join(path, y)  + " " + str(tag) + "\n", dataList[:int(ratioOfTrain * len(dataList))], "")
	configOfTest = reduce(lambda x, y: x + os.path.join(path, y) + " " + str(tag) + "\n", dataList[int(ratioOfTrain * len(dataList)):], "")
	
	trainFile.write(configOfTrain)
	testFile.write(configOfTest)


trainFile.close()
