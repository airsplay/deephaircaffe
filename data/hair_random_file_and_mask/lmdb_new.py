import numpy as np
import lmdb
import caffe
import cv2 as cv
import os
import random
from analysis import *
import hair_random

#Get the files for hair_cluster
parser = HairParser()
all_files = [x for x in hair_random.HairIterator(dict_form = True)]
random.shuffle(all_files)

f = open("train_input", "w")
for x in all_files:
	f.write(x['path'] + " " + x['file'] + "\n")


map_size = len(all_files) * 256*256* 4 * 4

print "The map_size", map_size
print "The total files are", len(all_files)

#exit()
#Create lmdb
env = lmdb.open("train_lmdb", map_size = map_size)

cnt = 0 
with env.begin(write=True) as txn:
	for d in all_files:
		l = random.randint(227, 256) # random get size from 227 to 256
		#print path
		if cnt % 500 == 0:
			print cnt
			#txn.commit()

		hair_file_name = os.path.join(d['path'], d['file'])
		mask_file_name = os.path.join(d['path'], d['mask'])

		hair = cv.resize(cv.imread(hair_file_name), (l, l))
		#show_image(hair)
		#mask = convert_mask01(cv.resize(cv.imread(mask_file_name, 0, ), (l, l)))
		mask = cv.resize(cv.imread(mask_file_name, 0), (l,l), interpolation = cv.INTER_NEAREST)/130
		#print mask
		#analyse_image(mask)
		#show_image(mask * 255)

		hair = hair.transpose(2, 0, 1)
		mask = mask[np.newaxis, :]
		img = np.concatenate((hair, mask), axis = 0)

		#print img.shape
		




		datum = caffe.proto.caffe_pb2.Datum()
		datum.channels = img.shape[0]
		datum.height = l
		datum.width = l 
		
		#print datum.channels, datum.height, datum.width

		#img = img.transpose(2, 0, 1)
			
		#datum.float_data.extend(img.reshape(img.size).tolist())
		datum.data = img.tobytes()

		str_id = '{:08}'.format(cnt)
		txn.put(str_id.encode('ascii'), datum.SerializeToString())

		cnt = cnt + 1
		#if cnt == 10:
			#break
print cnt
