python setup.py -m VGG_mask_deconvolution --data selected_file_and_mask_augment --source lmdb --batch_size=40 pseudo_pool_group=16
pause

python step_train.py --seg --save 100 -m snapshot/VGG_mask_deconvolution_selected_file_and_mask_augment/VGG_mask_deconvolution_selected_file_and_mask_augment_iter_2000.caffemodel 2>&1 | mtee train_log
pause