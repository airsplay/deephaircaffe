
import caffe
import numpy as np
import cv2 as cv
import lmdb
import matplotlib.pyplot as plt

from pylab import *
from PIL import Image
import os
import getopt
from analysis import *
from hairTools import *


if __name__ == "__main__":
	ifshow = False
	ifdeconv = False
	save_iter = 0
	show_iter = 0
	model = ""
	snapshot = ""

	opts, args = getopt.getopt(sys.argv[1:], "m:s:e:", ["experiment=", "model=", "snapshot=", "multiGPU", "show=", "save=", "deconv"])
	
	for op, value in opts:
		if op == "-s" or op == "--snapshot":
			snapshot = value
		if op == "-m" or op == "--model":
			model = value
		if op == "--show":
			ifshow = True
			show_iter = int(value)
		if op == "--save":
			save_iter = int(value)
		if op == "--deconv":
			ifdeconv = True
		if op == "-e" or op == "--experiment":
			experiment = value
			model = get_caffemodel(experiment)
			snapshot = get_solverstate(experiment)

	
	
	print "finish loading libraries"

	modelPath = "models"

	if False: #Reserve for multiple GPU
		caffe.set_device(0)
		caffe.set_device(1)
		caffe.set_device_count(2)

	caffe.set_mode_gpu()

	solver = caffe.SGDSolver(os.path.join(modelPath, 'solver.prototxt'))
	if snapshot != "":
		print "snapshot", snapshot
		solver.restore(snapshot)
	if model != "":
		print "model", model
		solver.net.copy_from(model)

	a = Analysis(net = solver.net)
	cnt = 0
	while True:
		solver.step(1)
		if (show_iter > 0 and (cnt % show_iter == 0)) or (save_iter > 0 and (cnt % save_iter == 0)):
			data = solver.net.blobs['data'].data[0]
			mask = solver.net.blobs['mask'].data[0][0]
			data = data.transpose(1,2,0)
			#mask = cv.resize(mask, (64, 64))
			#mask = mask * 255
			#mask.astype(np.uint8)
			predict = solver.net.blobs['out'].data[0]
			total = predict.size
			n = int(total ** 0.5)
			#print n
			predict = sigmoid(predict).reshape(n,n)
			#predict = cv.resize(predict, (64, 64))
			if (show_iter > 0 and (cnt % show_iter == 0)):
				a.save_conv_blobs("tmp")
				a.save_conv_filter("tmp")
				#a.save_deconv_filter("tmp")
				show_image(data / 255)
				show_image(mask)
				show_image(predict)
			if (save_iter > 0 and (cnt % save_iter == 0)):
				a.save_conv_blobs("tmp")
				a.save_conv_filter("tmp")
				save_image("tmp/%06ddata.png" % (cnt), data/255)
				save_image("tmp/%06dmask.png" % (cnt), mask)
				save_image("tmp/%06dpredict.png" % (cnt), predict)
				save_image("tmp/%06ddiscrete_predict.png" %(cnt), np.round(predict))
		cnt += 1
			
