:: The naive classifier
python setup.py -m alexnet_finetune --classify -d hair_random_hair_hair -s lmdb --batch_size=128
pause
python train.py -s snapshot/alexnet_finetune_hair_random_hair_hair/alexnet_finetune_hair_random_hair_hair_iter_2000.solverstate  2>&1 | mtee train_log

pause
