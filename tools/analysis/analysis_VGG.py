import shutil
import re
import numpy as np
import matplotlib.pyplot as plt
import os
import sys
import cv2 as cv
import caffe
from hairTools import *
from analysis import *

caffe_root = "../../"
model_folder = "models"
model = "alexnet_finetune"
snapshot_folder = "snapshot"
experiment = "alexnet_finetune_hair_constant"

#model_path = os.path.join(caffe_root, model_folder, model, "deploy.prototxt")
#model_path = "train_val.prototxt"
model_path = os.path.join(caffe_root, model_folder, "train_val.prototxt")
#caffemodel_path = os.path.join(caffe_root, snapshot_folder, "alexnet_finetune_hair_constant_20151022_all_layers", experiment + "_iter_144000.caffemodel")
#caffemodel_path = "D:\\airsplay\\HairTrain\\models\\alexnet_finetune\\" + "bvlc_alexnet.caffemodel"
#caffemodel_path = "D:\\airsplay\\HairTrain\\models\\VGG_finetune\\VGG_ILSVRC_16_layers.caffemodel"	
caffemodel_path = os.path.join(caffe_root, snapshot_folder, "VGG_finetune_mask_hair_origin", "VGG_finetune_mask_hair_origin_iter_" +"1000" + ".caffemodel")


class AnalysisVGG(Analysis):
	def __init__(self, model_path, caffemodel_path):
		Analysis.__init__(self, model_path, caffemodel_path)
			
	def show_figures(self):
		pass
		
	def show_mask(self, num = 0):
		raw_data = self.net.blobs['mask'].data[num]
		raw_data = raw_data[0]
		print raw_data.max()
		print np.average(raw_data)
		#image = self.transformer.deprocess('data', self.net.blobs['data'].data[num])
		#print "after deprocess", np.average(image)
		#plt.imshow(image)
		raw_data = raw_data * 255
		raw_data = raw_data.astype(np.uint8)
		print raw_data.max()
		print np.average(raw_data)
		#plt.imshow(self.net.blobs['data'].data[0])
		#plt.show()
		cv.namedWindow("Input data")
		cv.imshow("Input data", raw_data)
		cv.waitKey()	
		cv.destroyWindow("Input data")
	def save_feature_map(self):
		filters = self.net.params['conv1_1'][0].data
		plt.imsave('featureMap_conv1_1.png', vis_square(filters.transpose(0,2,3,1)))
	def save_figures(self):
		self.save_conv_blobs()
		img = self.net.blobs['dotfc7'].data[0][0]
		self.imsave('blob_last.png', img)
	def show_figures(self):
		for num in xrange(100):
			img = self.net.blobs['dotfc6'].data[0][num]
			show_image(img)
		

		
if __name__ == "__main__":	
	a = AnalysisVGG(model_path, caffemodel_path)
	a.show_blobs_shape()
	a.show_params_shape()
	a.forward()
	#a.show_input()
	#a.show_mask()
	a.save_conv_blobs()
	a.save_figures()
	a.show_figures()
	
	
