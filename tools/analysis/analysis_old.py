import numpy as np
import matplotlib.pyplot as plt
import os
import sys
import cv2 as cv

caffe_root = "../../"
model_folder = "models"
model = "alexnet_finetune"
snapshot_folder = "snapshot"
experiment = "alexnet_finetune_hair_constant"

model_path = os.path.join(caffe_root, model_folder, model, "deploy.prototxt")
#model_path = "train_val.prototxt"
caffemodel_path = os.path.join(caffe_root, snapshot_folder, "alexnet_finetune_hair_constant_20151022_all_layers", experiment + "_iter_144000.caffemodel")
#caffemodel_path = "D:\\airsplay\\HairTrain\\models\\alexnet_finetune\\" + "bvlc_alexnet.caffemodel"

testNum = 1


import caffe


caffe.set_mode_gpu()
net = caffe.Net(model_path,
				caffemodel_path,
				caffe.TEST)

t1 = caffe.io.Transformer({'data': net.blobs['data'].data.shape})
t1.set_transpose('data', (2,0,1))

transformer = caffe.io.Transformer({'data': net.blobs['data'].data.shape})
transformer.set_transpose('data', (2,0,1))
transformer.set_raw_scale('data', 255)
transformer.set_channel_swap('data', (2,1,0))


#net.blobs['data'].reshape(52, 3, 227, 227)

print caffe.io.load_image('test.png').shape
net.blobs['data'].data[...] = transformer.preprocess('data', caffe.io.load_image('test.png'))


out = net.forward()
#predict = out['prob'][0].argmax()
print "Predicted class is" , net.blobs['fc8_my'].data[0].argmax()

R = net.blobs['fc8_my'].data[0].argsort()
print R
#print "The true classfication is", net.blobs['label'].data[0]
#print out['prob'][0][predict]


#print out['fc8'][0]

# Show the image of input
print "before deprocess" , np.average(net.blobs['data'].data[0])
image = t1.deprocess('data', net.blobs['data'].data[0])
print "after deprocess", np.average(image)
plt.imshow(image)
image = image.astype(np.uint8)
#plt.imshow(net.blobs['data'].data[0])
#plt.show()
#cv.namedWindow("show")
#cv.imshow("show", image)
#cv.waitKey()


#exit()
# The shape of all blobs
for k, v in net.blobs.items():
	print k, v.data.shape

# All The params, 0 for parameter and 1 for bias
for k, v in net.params.items():
	print k, v[0].data.shape # This is the shape of all params

def vis_square(data,  padsize = 1, padval = 0):
	for i in xrange(data.shape[0]):
		data[i] -= data[i].min()
		data[i] /= data[i].max()

    # force the number of filters to be square
	n = int(np.ceil(np.sqrt(data.shape[0])))
	padding = ((0, n ** 2 - data.shape[0]), (0, padsize), (0, padsize)) + ((0, 0),) * (data.ndim - 3)
	#print padding
	data = np.pad(data, padding, mode='constant', constant_values=(padval, padval))
	#print data.shape
	#print data[0]
	
	# tile the filters into an image
	data = data.reshape((n, n) + data.shape[1:]).transpose((0, 2, 1, 3) + tuple(range(4, data.ndim + 1)))
	#print data.shape
	data = data.reshape((n * data.shape[1], n * data.shape[3]) + data.shape[4:])
	#print data.shape
	
	plt.imshow(data)
	#plt.show()
	return data


filters = net.params['conv1'][0].data
#plt.imsave('featureMap_conv1.png', vis_square(filters.transpose(0,2,3,1)))
filters = net.params['conv2'][0].data
#plt.imsave('featrueMap_conv2.png', vis_square(filters[:48].reshape(48**2,5,5)))

for layer in ['conv1', 'conv2', 'conv3', 'conv4', 'conv5']:
	feat = net.blobs[layer].data[0]
	plt.imsave('blob_' + layer + '.png', vis_square(feat, padval = 1))


for layer in ['fc6', 'fc7', 'fc8_my']:
	feat = net.blobs[layer].data[0]
	plt.subplot(2, 1, 1)
	plt.bar(range(len(feat)), feat.flat)
	plt.subplot(2, 1, 2)
	plt.hist(feat[feat > 0], bins = 100)
	plt.savefig("blob_" + layer + ".png")
	plt.clf()

feat = net.blobs['fc8_my'].data[0]
prob = np.exp(feat) / np.sum(np.exp(feat))#net.blobs['prob'].data[0]
plt.bar(range(len(prob)), prob)
plt.savefig("probability.png")
