from analysis import *
import numpy as np
from hairTools import *
import cv2 as cv

caffe_root = "../../"
model_folder = "models"
model = "alexnet_finetune_mask_google"
snapshot_folder = "snapshot"
experiment = "alexnet_finetune_mask_google_hair_origin_13"

model_path = os.path.join(caffe_root, snapshot_folder, experiment, "meta", "train_val.prototxt")
caffemodel_path = os.path.join(caffe_root, snapshot_folder, experiment, "alexnet_finetune_mask_google_hair_origin_iter_26000.caffemodel")

class AlexnetMask(Analysis):
	def __init__(self, model_path, caffemodel_path):
		Analysis.__init__(self, model_path, caffemodel_path)
	def get_predict(self, num = 0):
		predict = self.net.blobs['out'].data[num]
		predict = sigmoid(predict).reshape(13, 13)
		return predict
	def show_predict(self, num = 0):
		predict = self.get_predict(num)
		show_image(predict)
	def save_predict(self, num = 0):
		predict = self.get_predict(num)
		predict = np.round(predict) 
		save_image("predict.png", predict)
		
if __name__ == "__main__":
	a = AlexnetMask(model_path, caffemodel_path)
	a.show_blobs_shape()
	a.show_params_shape()
	a.forward()
	num = 1
	a.show_input(num)
	a.show_mask(num)
	a.show_predict(num)
	a.save_predict(num)
	a.save_mask(num)


