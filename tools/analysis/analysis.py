import shutil
import re
import numpy as np
import matplotlib.pyplot as plt
import os
import sys
import cv2 as cv
import caffe
import caffe_pb2
import google.protobuf
import math
from hairTools import *

caffe_root = "../../"
model_folder = "models"
model = "alexnet_finetune_mask_deconv_VGG_111"
snapshot_folder = "snapshot"
experiment = "alexnet_finetune_mask_deconv_VGG_227_hair_origin"

#model_path = os.path.join(caffe_root, snapshot_folder, experiment, "meta", "train_val.prototxt")
#model_path = os.path.join(caffe_root, model_folder, model, "deploy.prototxt")
#model_path = "train_val.prototxt"
model_path = os.path.join(caffe_root, model_folder, "train_val.prototxt")
#caffemodel_path = os.path.join(caffe_root, snapshot_folder, "alexnet_finetune_hair_gray_20151101", "alexnet_finetune_hair_gray_iter_12000.caffemodel")
#caffemodel_path = os.path.join(caffe_root, snapshot_folder, experiment, "alexnet_finetune_mask_google_hair_origin_iter_26000.caffemodel")
#caffemodel_path = ""
#caffemodel_path = get_caffemodel(experiment)
#caffemodel_path = "D:\\airsplay\\HairTrain\\snapshot\\alexnet_finetune_mask_deconv_VGG_55_hair_origin\\alexnet_finetune_mask_deconv_VGG_55_hair_origin_iter_4000.caffemodel"
#caffemodel_path = "D:\\airsplay\\HairTrain\\models\\alexnet_finetune\\" + "bvlc_alexnet.caffemodel"
caffemodel_path = ""





class Analysis: 
	def __init__(self, model_path = "", caffemodel_path = "", snapshot = "", net = None):
		caffe.set_mode_gpu()
		if net != None: # Explicit copy the net
			self.net = net
		else:
			if snapshot != "":
				self.snapshot = snapshot
				model_path = get_prototxt(snapshot)
				caffemodel_path = get_caffemodel(snapshot) 
				self.net_parameter = caffe_pb2.NetParameter()
				f = open(get_prototxt(snapshot))
				google.protobuf.text_format.Merge(f.read(), self.net_parameter)
				f.close()

			if caffemodel_path == "":		# No caffemodel 
				self.net = caffe.Net(model_path, caffe.TRAIN) #caffemodel_path, caffe.TRAIN) else: 
			else:
				self.net = caffe.Net(model_path, caffemodel_path, caffe.TRAIN)
		
		print model_path, caffemodel_path
		self.transformer = caffe.io.Transformer({'data': self.net.blobs['data'].data.shape})
		self.transformer.set_transpose('data', (2,0,1))
		#self.transformer.set_raw_scale('data', 255)
		if self.net.blobs['data'].data.shape[1] == 3: # For an RGB image input
			print "Set channel swap"
			#self.transformer.set_channel_swap('data', (2,1,0))
			pass


		#self.net.blobs['data'].reshape(52, 3, 227, 227)
	
	# There's bug with the official reshape of Net.
	# Rewrote it.
	def reshape(self):
		for l in self.net_parameter.layer:
			name_bottom = l.bottom[0]
			name_top = l.top[0]
			print l.name, name_bottom, name_top
			batch_size, old_c, old_h, old_w = self.net.blobs[name_bottom].data.shape
			blob_top = self.net.blobs[name_top]
			if l.type == "Convolution":
				print l.name
				conv_f = lambda h, p, k, s: (h + 2 * p - k) / s + 1
				param = l.convolution_param
				new_c = param.num_output
				new_h = conv_f(old_h, param.pad[0], param.kernel_size[0], param.stride[0])		
				new_w = conv_f(old_w, param.pad[0], param.kernel_size[0], param.stride[0])
			if l.type == "Pooling":
				pool_f = lambda h, p, k, s: int(math.ceil(float(h + 2 * p - k) / s) + 1)
				param = l.pooling_param
				new_c = old_c
				new_h = pool_f(old_h, param.pad, param.kernel_size, param.stride)		
				new_w = pool_f(old_w, param.pad, param.kernel_size, param.stride)
			if l.type == "Deconvolution":
				param = l.convolution_param
				new_c = param.num_output
				deconv_f = lambda h, p, k, s: h * (h-1) + k - 2*p
				new_c = param.num_output
				new_h = deconv_f(old_h, param.pad[0], param.kernel_size[0], param.stride[0])
				new_w = deconv_f(old_w, param.pad[0], param.kernel_size[0], param.stride[0])
		
			blob_top.reshape(batch_size, new_c, new_h, new_w)
	# Load data from path "data"
	# Forward the network from the data layer(without extract data from file)
	def load_data(self, data):
		if isinstance(data, str):
			self.net.blobs['data'].data[...] = self.transformer.preprocess('data', caffe.io.load_image(data))
		else:
			self.net.blobs['data'].data[...] = self.transformer.preprocess('data', data)
		if 'mask' in self.net.blobs.keys():
			self.net.forward(start = ['data', 'mask'])
		else:
			self.net.forward(start = 'conv1')

	# Forward the network
	def forward(self, **kwargs):
		out = self.net.forward(**kwargs)
		#result = self.net.blobs['fc8_my'].data[0].argmax()
		#print "Predicted class is" , result
		#R = self.net.blobs['fc8_my'].data[0].argsort()
		#print "Potential", R[:8]
		#return result, R

	# Show the image of input
	def show_input(self, num = 0):
		#print "before deprocess" , np.average(self.net.blobs['data'].data[0])
		raw_data = self.net.blobs['data'].data[num]
		#print raw_data.max()
		#print np.average(raw_data)
		image = self.transformer.deprocess('data', raw_data)
		#print "after deprocess", np.average(image)
		#plt.imshow(image)
		image = image.astype(np.uint8)
		#print image.max()
		#print np.average(image)
		#plt.imshow(self.net.blobs['data'].data[0])
		#plt.show()
		show_image(image)
	
	# You could rewrite this method to use show_mask and save_mask correctly
	def get_mask(self, num = 0):
		raw_data = self.net.blobs['mask'].data[num][0]
		return raw_data

	def show_mask(self, num = 0):
		raw_data = self.get_mask(num)


	def save_mask(self, num = 0):
		raw_data = self.get_mask(num)
		save_image("mask.png", raw_data)
	
	def size_of_blobs(self):
		total = 0
		for k, v in self.net.blobs.items():
			total += v.data.size
		return total
	
	def size_of_params(self):
		total = 0 
		for k,v in self.net.params.items():
			total += v[0].data.size
		return total

	# Show the shape of all blobs
	def show_blobs_shape(self):
		print "The shape of blobs:"
		# The shape of all blobs
		for k, v in self.net.blobs.items():
			print "blob", k, ":", v.data.shape
		print ""

	# Show the shape of all params
	def show_params_shape(self):
		print "The shape of params:"
		# All The params, 0 for parameter and 1 for bias
		for k, v in self.net.params.items():
			print k, v[0].data.shape # This is the shape of all params
		print ""
	
	def memory(self):
		return 240000000 + self.size_of_blobs() * 10 + self.size_of_params() * 9

	def save_input(self, path = ""):
		#print "before deprocess" , np.average(self.net.blobs['data'].data[0])
		image = self.transformer.deprocess('data', self.net.blobs['data'].data[0])
		#print "after deprocess", np.average(image)
		#plt.imshow(image)
		#image = image.astype(np.uint8)
		#plt.show()
		#cv.namedWindow("Input data")
		#cv.imshow("Input data", image)
		#cv.waitKey()
		#plt.imsave(os.path.join(path, "origin.png"), image)
		save_image(os.path.join(path, "origin.png"), image)
	
	def save_raw_input(self, path = "", num = 0):
		image = self.net.blobs['data'].data[num].transpose(1,2,0)
		save_image(os.path.join(path, "raw_origin.png"), image)
	
	# It saves the image of output layer.  
	# If the image is an RGB image, save it as RGB.
	# If the image has only one channel then save it in gray-scale
	def save_output(self, path = "", name = "out.png"):
		out = self.net.blobs['out'].data[0].copy()
		if out.shape[0] == 3:	#	Suggest that it is an RGB image
			out = self.transformer.deprocess('data', out)
		else:					# Reinterpret it as an gray image
			n = int(out.size ** 0.5)
			out = out.reshape(n, n)
		
		save_image(os.path.join(path, name), out)
	
	# It shows the image of output layer.  
	# If the image is an RGB image, show it as RGB.
	# If the image has only one channel then show it in gray-scale
	def show_output(self, *args, **kwargs):
		out = self.net.blobs['out'].data[0].copy()
		if out.shape[0] == 3:
			out = self.transformer.deprocess('data', out)
		else:
			n = int(out.size ** 0.5)
			out = out.reshape(n, n)

		show_image(out, *args, **kwargs)
			
			


	# Save the feature map of the first two convolultion layers
	def save_feature_map(self, path = ""):
		self.save_conv_filter(path)

	# Attention: denpends on the layer's name
	# Save the filter of the first two convolultion layers
	def save_conv_filter(self, path = ""):
		filters = self.net.params['conv1'][0].data.copy()
		plt.imsave(os.path.join(path, 'filter_conv1.png'), vis_square(filters.transpose(0,2,3,1)))
		filters = self.net.params['conv2'][0].data.copy()
		plt.imsave(os.path.join(path, 'filter_conv2.png'), vis_square(filters[:48].reshape(48**2,5,5)))
	
	# Attetion: dependen on the layer's name
	# Save the filter of the last deconv layers
	def save_deconv_filter(self, path = ""):
		filters = self.net.params['deconv7'][0].data.copy()
		filters = filters.transpose(1,0,2,3)[0]
		plt.imsave(os.path.join(path, 'filter_deconv7'), vis_square(filters))
	
	def show_raw_filter(self, layer, cnt):
		flt0 = self.net.params[layer][0].data.copy()
		flt1 = self.net.params[layer][1].data.copy()
		print flr0
		print flt1

	# Save the blob generated by all the convolution & pooling layers.
	# The blob of deconv layer also added.
	# A new version which could show the order of each blobs
	def save_conv_blobs(self, path = ""):
		cnt = 0 
		for layer in filter(lambda x:"conv" in x or "pool" in x, self.net.blobs.keys()):
			feat = self.net.blobs[layer].data[0].copy()
			plt.imsave(os.path.join(path, ('%02d_' % (cnt)) + 'blob_' + layer + '.png'), vis_square(feat, padval = 1))
			cnt += 1

	# Save the blob data generated by all the fully connection layer.
	# Suppose all the fully connection layer are guided by "fc"
	# Two graphs will be created:
	# 	1. The result of each gate
	#	2. The distribution of activation
	def save_fc_blobs(self, path = ""):
		for layer in filter(lambda x:("fc" in x) , self.net.blobs.keys()):
			feat = self.net.blobs[layer].data[0]
			feat = feat.reshape(-1) #Force the feature to be a vector, -1 means default
			plt.subplot(2, 1, 1)
			plt.bar(range(len(feat)), feat.flat)
			plt.subplot(2, 1, 2)
			plt.hist(feat[feat > 0], bins = 100) #The fc layer always perform RELU, so 0 is meaningless
			plt.savefig(os.path.join(path, "blob_" + layer + ".png"))
			plt.clf()

	# Attention: Inconsistency.
	# Save the bar of probability 
	def save_prob(self, path = ""):
		feat = self.net.blobs['out'].data[0]
		# Calculate the probablity by softmax
		prob = np.exp(feat) / np.sum(np.exp(feat))
		plt.bar(range(len(prob)), prob)
		plt.savefig(os.path.join(path, "probability.png"))
	
	def imsave(self, fileName, img):
		print img.max()	
		print np.average(img)
	
	
		

def vis_square(data,  padsize = 1, padval = 0):
	for i in xrange(data.shape[0]):
		#print data[i].max()
		#print np.average(data[i])
		data[i] -= data[i].min()
		data[i] /= data[i].max()

    # force the number of filters to be square
	n = int(np.ceil(np.sqrt(data.shape[0])))
	padding = ((0, n ** 2 - data.shape[0]), (0, padsize), (0, padsize)) + ((0, 0),) * (data.ndim - 3)
	data = np.pad(data, padding, mode='constant', constant_values=(padval, padval))
	
	# tile the filters into an image
	data = data.reshape((n, n) + data.shape[1:]).transpose((0, 2, 1, 3) + tuple(range(4, data.ndim + 1)))
	data = data.reshape((n * data.shape[1], n * data.shape[3]) + data.shape[4:])
	
	plt.imshow(data)
	#plt.show()
	return data


class HairParser:
	def __init__(self):
		self.regex = re.compile(r'strands(\d+)_(\d+)') 
		self.file_regex = re.compile(r'strands(\d+)_(\d+).png$')
		self.hair_regex = re.compile(r'strands(\d+)_(\d+)_hair.png')
		self.mask_regex = re.compile(r'strands(\d+)_(\d+)_mask.png')
		self.gray_regex = re.compile(r'strands(\d+)_(\d+)_gray.png')
	def parse(self, s):
	# Parse the s into label and cnt
		return map(int, self.regex.match(s).groups())
	def get_path(self, label):
		# Get the folder name
		return "strands%05d" % (label)
	def match(self, s):
		# If the parameter s match the 
		if self.file_regex.match(s) != None:
			return True
		else:
			return False
	def match_hair(self, s):
		# If the parameter match the file format of hair
		if self.hair_regex.match(s) != None:
			return True
		else:
			return False
	def match_gray(self, s):
		# If the parameter s match the file format of gray image
		if self.gray_regex.match(s) != None:
			return True
		else:
			return False
	def match_mask(self, s):
		# If the parameter s match the file format of mask image
		if self.mask_regex.match(s) != None:
			return True
		else:
			return False
			
	def file2mask(self, s):
		label, cnt = self.parse(s)
		return "strands%05d_%04d_mask.png" %(label, cnt)
	def file2mask01(self, s):
		return self.file2any(s, "mask01")
	def file2hair(self, s):
		label, cnt = self.parse(s)
		return "strands%05d_%04d_hair.png" %(label, cnt)
	def file2gray(self, s):
		label, cnt = self.parse(s)
		return "strands%05d_%04d_gray.png" %(label, cnt)
	def file2any(self, s, attribute):
		label, cnt = self.parse(s)
		return "strands%05d_%04d_%s.png" %(label, cnt, attribute)
	def get_file(self, label, cnt):
		# Get the file name
		return "strands%05d_%04d.png" % (label, cnt)
			
		

class HairLabelTransformer:
	def __init__(self):
		f = open ("D:\\airsplay\\HairTrain\\data\\hair\\transform.txt", 'r')
		self.r2c = dict(map(lambda x:map(int, x[:-1].split()), f.readlines()))
		#print self.r2c
		self.c2r = dict(zip(self.r2c.values(), self.r2c.keys()))
		#print self.c2r
		
		f.close()
		
	def real2caffe(self, a):
		if self.r2c.has_key(a): #Two data haven't been downloaded correctly TAT
			return self.r2c[a]
		else:
			return 0	
	def caffe2real(self, a):
		if self.c2r.has_key(a):
			return self.c2r[a]
		else:
			return 1
		
def pure_analysis(model):
	#tmp = os.path.join(caffe_root, model_folder, model, "deploy.prototxt")
	tmp = os.path.join(caffe_root, model_folder, "train_val.prototxt")
	a = Analysis(tmp, "")
	a.show_blobs_shape()
	a.show_params_shape()
	print "The number of data are ", a.size_of_blobs()
	print "The number of params are ", a.size_of_params()
	print "The approxiate memory is ", a.memory(), "B"
if __name__ == "__main__":	
	#a = Analysis(model_path, caffemodel_path)
	pure_analysis("alexnet_finetune_map_deconv_alexnet_55_no_pooling")
	
	#a.forward()
	#a.show_input()
	
