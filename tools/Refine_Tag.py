# Refine the input's label to 0 -- max
# Output to trainInput_tmp and testInput_tmp
# You should mannuly check the correctness of these files and change the names
# I prefer change the trainInput to trainInput_origin and change trainInput)tmp to trainInput.

# Maybe I will make this tool easy to use someday.

data_name = "hair"

import os

class Refine:
	def __init__(self, data_name):
		self.data_path = "../data"
		self.data_name = data_name

	def _out(self, m):
		f = open("transform.txt", 'w')
		for k, v in m.items():
			print k, v
			f.write(str(k) + " " + str(v) + "\n")	
			
	def _refine(self, fileName):
		f = open(fileName, "r")
		s = f.readlines()
		f.close()
		s = map(lambda x:x[:-1].split(" "), s)
		files, tags = zip(*s)			# parse the files' name and tags
		
		tagSet = list(set(tags))		# Make the tags unique
		print len(tagSet)
		
		tagSet = map(int, tagSet)		# Change it to int for sorting
		tagSet.sort()					# To preserve the property of tags better
		tagSet = map(str, tagSet)
		

		tagMap = dict(zip(tagSet, range(len(tagSet))))			# create the Map the tags to 0..(tagSum-1)
		
		self._out(tagMap)
		return
		
		print "Create the map"
		tags = map(lambda x:str(tagMap[x]), tags)					# map the tags

		print "Generate the output"
		out = [files[i] + " " + tags[i] + "\n" for i in xrange(len(files))]

		print "Write the output"
		f = open(fileName + "_tmp", "w")
		f.writelines(out)
		f.close()
		
		
		
	def refine(self):
		self._refine(os.path.join(self.data_path, self.data_name, "trainInput_origin"))
		#self._refine(os.path.join(self.data_path, self.data_name, "testInput"))


if __name__ == "__main__":
	refine = Refine(data_name)
	refine.refine()
