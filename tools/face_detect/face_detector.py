import cv2 as cv
import os
import numpy as np
from hairTools import *
class FaceDetector:
	def __init__(self):
		self.face_cascade = cv.CascadeClassifier()
		self.file_path = os.path.split(os.path.realpath(__file__))[0]
		self.face_cascade.load(os.path.join(self.file_path, "haarcascade_frontalface_alt2.xml"))
		self.profile_face_cascade = cv.CascadeClassifier()
		self.profile_face_cascade.load(os.path.join(self.file_path, "haarcascade_profileface.xml"))
	
	# The code provided by Chongyang & Yizhou
	# I don't know what it did :)
	def detect(self, frame):
		h, w, c =  frame.shape	
		#show_image(frame)
		minFaceH = max(50, int(0.075 * h))
		pyrFrame = cv.pyrDown(frame)
		faces = self.face_cascade.detectMultiScale(pyrFrame, 1.2, 2, 0, (minFaceH,) * 2)
		profile_faces = self.profile_face_cascade.detectMultiScale(pyrFrame, 1.2, 2, 0, (minFaceH,) * 2)
		#if (faces)
		print faces
		print profile_faces
		#faces.extends(profile_faces)
		
		
		#print np.append(faces, profile_faces, axis = 0)
		#if len(faces) == 0:
			#faces = profile_faces

		return [(l*2, u*2, (l+r)*2, (u+d)*2)for l, u, r, d in faces]
		#return faces


	# The default resize process
	# Resize the whole img to default_size
	def default_resize(self, img, default_size):
		w, h, c = img.shape
		avg = (w + h) / 2
		scale = 1. * default_size / avg
		new_h = int(h * scale)
		new_w = int(w * scale)
		new_shape = (new_h, new_w)
		return cv.resize(img, new_shape)
		

	# If the face has been detected. Resize the face region to default_resize
	def auto_resize(self, img, default_size = 224):
		faces = self.detect(img)
		if len(faces) == 0:
			print "No face there!!"
			return img
			#retjrn self.default_resize(img, default_size)
		#self.show_result(img, faces)	
		l, u, r, d = faces[0]
		face_h = d - u
		face_w = r - l
		face_avg = (face_h + face_w) / 2
		# 0.7 is a approximate proportion of head in range default_size
		scale = 0.7 * (default_size) / face_avg

		w, h, c = img.shape
		new_h = int(h * scale)
		new_w = int(w * scale)
		#new_c = c
		new_shape = (new_h, new_w)

		return cv.resize(img, new_shape)
	
	def show_result(self, img, faces):
		for face_range in faces:
			draw_rectangle(img, *face_range)
			face_range = [i*2 for i in face_range]
			l, u, r, d = face_range
			r = l+r
			d = u+d
		show_image(img)
		
def draw_rectangle(img, l, u, r, d, color = 0):
	print l,r,u,d
	#show_image(img)
	rgb =  cv.cvtColor(np.uint8([[[color, 128, 128]]]), cv.COLOR_HSV2BGR)[0][0]
	rgb = tuple(rgb.tolist())
	x,y,z = rgb
	print rgb
	print x, y, z


	return cv.rectangle(img, (l,u), (r,d), (x,y,z), thickness = 1)

if __name__ == "__main__":
	fd = FaceDetector()
	image_names = os.listdir("test_set")	
	os.chdir("test_set")
	for file_name in image_names:
		img = cv.imread(file_name)
		print img.shape
		img = fd.auto_resize(img)
		analyse_image(img)
		show_image(img)
		#faces = fd.detect(img)
			
		


		
		
		

		
