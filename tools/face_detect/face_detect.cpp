bool compareRect(cv::Rect r1, cv::Rect r2) { return r1.height 

	cv::Rect detectFace(const cv::Mat& frame, cv::CascadeClassifier& face_cascade, const int frameHeight) 
	{
		int minFaceH = std::max
			(50,(int)(0.075*frameHeight));
		cv::Size minFace(minFaceH, minFaceH); // minimum face size to detect

		cv::Mat frame2;
		pyrDown(frame, frame2); 
		std::vector
			faces;
		face_cascade.detectMultiScale(frame2, faces, 1.2, 2, 0, minFace);

		if (faces.empty())
			return cv::Rect(0,0,0,0);

		cv::Rect faceL = *max_element(faces.begin(),faces.end(),compareRect);
		faceL.x *= 2; faceL.y *= 2; faceL.width *= 2; faceL.height *= 2;

		return faceL;
	}

	cv::CascadeClassifier face_cascade;

	face_cascade.load("haarcascade_frontalface_alt2.xml");

	cv::Rect faceL = detectFace(portrait_image, face_cascade, portrait_image.cols);

