#----------Used to transfer the data--------#
import os
import sys
import analysis
import numpy as np
import cv2 as cv
import re
import matplotlib.pyplot as plt
#import bounding_box
caffe_root = "D:/airsplay/HairTrain"
#model_folder = "models"
snapshot_folder = "snapshot"
DEFAULT_LABEL = 341	# The label for backgound
def sigmoid(vec):
	return 1.0 / (1 + np.exp(-vec))
def show_image(img, normalized = False):
	#print img.max()
	#print np.average(img)
	if normalized:
		img = img * 1.0 / img.max()
	cv.namedWindow("show")
	cv.imshow("show", img)
	cv.waitKey()
	cv.destroyWindow("show")
# Use openCV here
# The plt saves image using a color bar while openCV use raw data
def save_image(name, img):
	#print name
	#print img.max()
	#print np.average(img)
	if img.max() <= 1:
		#print "scale 255"
		cv.imwrite(name, img * 255)
	else:
		cv.imwrite(name, img)


# Convert all the >0 to 1
def convert_mask01(img):	
	n = img.shape[0]
	m = img.shape[1]
	for i in xrange(n):
		for j in xrange(m):
			if img[i][j] > 0:
				img[i][j] = 1
	return img

# The plt saves image using a color bar while openCV use raw data
def save_image_plt(name, img):
	plt.imsave(name, img) 
def show_image_plt(img):
	plt.imshow(img)
	plt.show()
def analyse_image(img, msg = ""):
	print msg
	print "The shape of the image ", img.shape
	print "The min is", img.min(), "The max is", img.max()
	print "The average of the image is", np.average(img)
	print "All the values in the images are", np.unique(img)
	print ""
# The data format is used in CAFFE, which is N * C * W * H
# The image is C * W * H and the channel indicate the probability map
def convert_indicator_image(data):
	return data.argmax(axis = 0)


def convert_second_indicator_image(data):
	max_tmp = data.argmax(axis = 0)
	data_tmp = data.copy()
	n = data_tmp.shape[1]
	m = data_tmp.shape[2]
	for i in xrange(n):
		for j in xrange(m):
			data_tmp[max_tmp[i][j]][i][j] = 0
	return data_tmp.argmax(axis = 0)
# The data format is used in CAFFE, which is N * C * W * H
# The image is C * W * H and the channel indicate the probability map
def convert_probability_image(data):
	data = np.exp(data)
	data_max = data.max(axis = 0)
	data_sum = data.sum(axis = 0)
	return data_max / data_sum

#Read the image from format ppm
def read_ppm(name):
	f = open(name)
	a = f.readlines()
	h, w, ran = map(int, a[0].split()[1:])
	b = map(ord, a[1])
	b = np.array(b)
	b = b.reshape([h, w, 3])
	return b


def get_caffemodel(experiment):
	experiment_path = os.path.join(caffe_root, snapshot_folder, experiment)
	files = os.listdir(experiment_path)
	tmp = 0
	result = ""
	for f in files:
		finding = re.search(r'iter_(\d+)', f)
		if finding == None:
			continue
		else:
			cnt = int(finding.groups()[0])
		if cnt >= tmp:
			if os.path.splitext(f)[1] == ".caffemodel":
				result = f
			tmp = cnt
	return os.path.join(experiment_path, result)
def get_solverstate(experiment):
	experiment_path = os.path.join(caffe_root, snapshot_folder, experiment)
	files = os.listdir(experiment_path)
	tmp = 0
	result = ""
	for f in files:
		finding = re.search(r'(\d+)', f)
		if finding == None:
			continue
		else:
			cnt = int(finding.group())
		if cnt >= tmp:
			if os.path.splitext(f)[1] == ".solverstate":
				result = f
			tmp = cnt
	return os.path.join(experiment_path, result)			
			
def get_prototxt(experiment):
	return os.path.join(caffe_root, snapshot_folder, experiment, "meta", "models", "train_val.prototxt")

def get_hair(folder_path):
	file_list = os.listdir(folder_path)
	parser = analysis.HairParser()
	for fileName in file_list:
		if parser.match(fileName):
			maskName = parser.file2mask(fileName)
		else:
			continue

		filePath = os.path.join(folder_path, fileName)
		maskPath = os.path.join(folder_path, maskName)
		hairPath = os.path.join(folder_path, parser.file2hair(fileName))
		
		fileImg = cv.imread(filePath)
		maskImg = cv.imread(maskPath)

		hairImg = fileImg & maskImg

		#show_image(hairImg)

		cv.imwrite(hairPath, hairImg)

def get_gray(folder_path):
	file_list = os.listdir(folder_path)
	parser = analysis.HairParser()
	for fileName in file_list:
		if parser.match(fileName):
			maskName = parser.file2mask(fileName)
		else:
			continue

		filePath = os.path.join(folder_path, fileName)
		grayPath = os.path.join(folder_path, parser.file2gray(fileName))
		
		fileImg = cv.imread(filePath)
		grayImg = cv.cvtColor(fileImg, cv.COLOR_BGR2GRAY)


		cv.imwrite(grayPath, grayImg)



		
		
def check_or_create(path):
# If the path not exist, then create it
	if os.path.exists(path):
		return
	os.mkdir(path)

def create_new_input(origin, t):
	f = open("trainInput", 'w')
	for line in open(origin):
		path, label = line.split()
		d,name = os.path.split(path)
		new_name = t(name)
		f.write(os.path.join(d, new_name) + " " + label + "\n")
		
if __name__ == "__main__":
	#get_hair("D:\\airsplay\\HairTrain\\test\\rand_viewpoint")
	#get_gray("D:\\airsplay\\HairTrain\\test\\rand_light")
	experiment = "alexnet_finetune_hair_gray_20151101_typicall"
	print get_caffemodel(experiment), get_prototxt(experiment)
		
