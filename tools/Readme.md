#Tools

Here are some tools.
##parse_log
First put your `train_log` in this folder. using `parse_log.py` or just use `parse.bat` to generate log. They are *train_log.test.csv* and *train_log.train.csv*.
A matlab program `show.m` will draw the graph. This program dependes on what your network is.

##analysis 
The kernel of analysis of caffe, which is the base class of many other py program.

##auto_decaf(Not compelete)



##convert_imageset
Please compile `caffe/tools/convert_imageset.cpp` into a folder contained in PATH such as `caffe/bin`


This tool convert the a imageset in folder into lmdb or leveldb. (default choice is lmdb).

The origin program need 1T free space. I change the value of `LMDB_MAP_SIZE` in `caffe/src/util/db_lmdb.cpp` to bypass this problem.

###Usage
```bash
	conver_imageset "" path/to/train.txt path/to/output
```


##Refine_Tag.py
It map the tags from an oridinary order into 0..tagNum

##hairTools.py
Many useful tools.