import cv2 as cv
import numpy as np
from hairTools import *
def image_adjust(img):
	fs = 227 #Short for fixed_size
	h,w,c = img.shape
	#show_image(img)
	if h > w:
		new_h = fs
		new_w = int(1. * w / h * fs)
		print new_h, new_w
		img = cv.resize(img, (new_w, new_h)) 
		pad_left = (fs - new_w) / 2
		pad_right = fs - new_w - pad_left
		img = np.pad(img, ((0,0), (pad_left, pad_right), (0,0)), mode = 'constant')
		#show_image(img)
	else:
		new_h = int(1. * h / w * fs)
		new_w = fs
		print new_h, new_w
		img = cv.resize(img, (new_w, new_h)) 
		pad_left = (fs - new_h) / 2
		pad_right = fs - new_h - pad_left
		img = np.pad(img, ((pad_left,pad_right), (0, 0), (0,0)), mode = 'constant')
		#show_image(img)
	return img

def bounding_box(img, f):
	n = img.shape[0]
	m = img.shape[1]

	#l,r,u,d = 
	#if isinstance(f):
	for i in xrange(n):
		if f in img[i,:]:
			break
			#print i
	u = i# print i
	for i in range(n)[::-1]:
		if f in img[i,:]:
			break
	d = i#print i

	for j in xrange(m):
		if f in img[:,j]:
			break
	l = j
	for j in range(m)[::-1]:
		if f in img[:,j]:
			break
	r = j

	return (l,r,u,d)
			

if __name__ == "__main__":
	img = cv.imread("cat.jpg")
	analyse_image(img)
	x = bounding_box(img, 23)
	print x
	#image_adjust(img)
