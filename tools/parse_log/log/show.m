k = 100000000;
iter_per_epochs = 46800 / 128;


a = readtable('train_log.test.csv');
figure(1)
x = a.iters(a.iters < k) / iter_per_epochs;
a1 = a.accuracy_1(1:length(x));
plot(x, a1);
hold on;
a5 = a.accuracy_5(1:length(x));
plot(x, a5);
xlabel('Iterations')

%ylabel('Accuracy')


b = readtable('train_log.train.csv');

%figure(2)

x = b.iters(b.iters < k);
x = x / iter_per_epochs;
y = b.loss(1:length(x));
plot(x, y / max(y))
hold on;
xlabel('Epoch')
%ylabel('Loss')

plot(x, b.learningRate / max(b.learningRate))
legend('Accuracy_1', 'Accuracy_5', 'Normalized loss', 'Normalized Learning Rate');