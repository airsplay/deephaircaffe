k = 100000000;
iter_per_epochs = 76800 / 45;

b = readtable('train_log.train.csv');

%figure(2)

x = b.iters(b.iters < k);
x = x / iter_per_epochs;
y = b.loss(1:length(x));
plot(x, y)
hold on;
xlabel('Epoch')
%ylabel('Loss')

x1 = 1:(x(length(x)) + 1);
y1 = zeros(size(x1));
cnt = zeros(size(x1));
for i = 1:length(x)
    tmp = floor(x(i));
    y1(tmp) = y1(tmp) + y(i);
    cnt(tmp) = cnt(tmp) + 1;
end
y1 = y1 ./ cnt;
plot(x, b.learningRate / max(b.learningRate))
plot(x1, y1)
plot(x, b.accuracy)

legend('Normalized loss', 'Normalized Learning Rate', 'Smooth Loss');