k = 100000000;
%{
a = readtable('train_log.test.csv');
figure(1)
x = a.iters(a.iters < k);
y = a.accuracy(1:length(x));
plot(x, y)
xlabel('Iterations')
hold on;
legend('Accuracy');
%ylabel('Accuracy')
%}

a = readtable('train_log.train1.csv');
x1 = a.iters(a.iters <k);
y1 = a.loss(1:length(x1));

b = readtable('train_log.train.csv');
x2 = b.iters(b.iters < k);
y2 = b.loss(1:length(x2));
%figure(2)

x = [x1; x2];
y = [y1; y2];

iter_per_epochs = 1000 / 31;

x = x / iter_per_epochs;

plot(x, y / max(y))
hold on;
xlabel('Epoch')
%ylabel('Loss')

plot(x, [a.learningRate; b.learningRate] / max(a.learningRate))
legend('Normalized loss', 'Normalized Learning Rate');