from matplotlib import pyplot as plt
import numpy as np
import cv2 as cv

# For each element in a, judge through oracle
def array_oracle(a, oracle):
	
	for i in a:
		if oracle(i):
			#print oracle(i)
			return True
	return False

		
# The bounding_box of an oracle
# While oracle is an lambda function
def bounding_box_oracle(img, oracle):
	n = img.shape[0]
	m = img.shape[1]
	i = 0
	j = 0
	for i in xrange(n):
		if array_oracle(img[i,...], oracle):
			break
	u = i
	for i in range(n)[::-1]:
		if array_oracle(img[i,...], oracle):
			break
	d = i
	for j in xrange(m):
		if array_oracle(img[:,j,...], oracle):
			break
	l = j
	for j in range(m)[::-1]:
		if array_oracle(img[:,j,...], oracle):
			break
	r = j
	
	return (l, r, u, d)

def bounding_box_nonzero(img):
	if len(img.shape) >= 2:
		img = img.sum(axis = 2)#img == 0#(img == [255, 255, 255, 255])
	img > 0
	b = np.argwhere(img)
	(u,l), (d,r) = b.min(0), b.max(0) + 1
	#print (u,l,d,r)
	return (u, l, d, r)

# IF the larger axis of img > l, shrunk of image. do nothing otherwize
def fit_max_to(img, l):
	n = img.shape[0]
	m = img.shape[1]
	if (n > l) or (m > l):
		if (n > m):
			img = cv.resize(img, (m*l/n, l))
		else:
			img = cv.resize(img, (l, n*l/m))
	return img

# Force the larger axis of img to l
def adjust_max_to(img, l):
	n = img.shape[0]
	m = img.shape[1]
	if (n > m):
		img = cv.resize(img, (m*l/n, l))
	else:
		img = cv.resize(img, (l, n*l/m))
	return img

# Change the img to the center of a large image
# If the img is large then l*l, shrunk the longest axis to l, then pad it
def fill_center(img, l):
	img = fit_max_to(img, l)	
	n = img.shape[0]
	m = img.shape[1]
	u_pad = (l - n) / 2
	d_pad = (l - n - u_pad)

	l_pad = (l - m) / 2
	r_pad = l - m - l_pad

	padding = ((u_pad, d_pad), (l_pad, r_pad)) + ((0, 0), ) * (img.ndim - 2)
	#print padding
	img = np.pad(img, padding, mode = "constant", constant_values = (0, 0))
	return img

# First change the img to about size norml,
# Then fill center in  l 
# norml <= l
def fill_center_normalize(img, norml, l):
	if norml > l:
		print 
	img = adjust_max_to(img, norml)
	# Copy the code from above for robust and speed
	n = img.shape[0]
	m = img.shape[1]
	u_pad = (l - n) / 2
	d_pad = (l - n - u_pad)

	l_pad = (l - m) / 2
	r_pad = l - m - l_pad

	padding = ((u_pad, d_pad), (l_pad, r_pad)) + ((0, 0), ) * (img.ndim - 2)
	#print padding
	img = np.pad(img, padding, mode = "constant", constant_values = (0, 0))
	return img
	

def is_rgb(x, r, g, b):
	if x[0] == r and x[1] == g and x[2] == b:
		return True
	return False
# The pixel is white
def is_white(x):
	if (x[0] == 255) and (x[1] == 255) and (x[2] == 255):
		return True
	return False

def not_white(x):
	return not(is_white(x))
# The pixel is not black
def not_black(x):
	sum(x) > 0


def show_image(img):
	plt.imshow(img)
	plt.show()
if __name__ == "__main__":
	a = plt.imread("test.gif")
	a = a[...,:3]
	print a.shape
	#show_image(a)
	#bb = bounding_box_oracle(a, not_white)
	a = fill_center(a, 1000)
	show_image(a)
	bb = bounding_box_nonzero(a)
	print bb
